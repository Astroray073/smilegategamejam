﻿using UnityEngine;
using System.Collections;

public class stone : MonoBehaviour {
    private CP.Plane current_plane;
    public GameManager manager;

	// Use this for initialization
	void Start () {
        manager = GameManager.Gamemanager;
        current_plane = manager.Map.PlaneOnFloor[manager.Map.PlaneOnFloor.Count - 1][0]; //MapGenerator에서 맨 위 위치 가져옴
        transform.position = new Vector3(current_plane.Center.x, current_plane.Center.y, -1);//top 위치
        StartCoroutine(Throw_stone());
	}
	
    IEnumerator Throw_stone()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.0f);

            int n;
            do
            {
                n = (int)Random.Range(0, 2.1f);
                switch (n)
                {
                    case 0:
                        {
                            if (current_plane.BottomPlane == null)
                            {
                                Destroy(gameObject);
                                yield return new WaitForFixedUpdate();
                            }
                            current_plane = current_plane.BottomPlane;
                            n += 10;
                            break;
                        }
                    case 1:
                        {
                            if (current_plane.LeftPlane == null)
                                continue;
                            current_plane = current_plane.LeftPlane;
                            n += 10;
                            break;
                        }
                    case 2:
                        {
                            if (current_plane.RightPlane == null)
                                continue;
                            current_plane = current_plane.RightPlane;
                            n += 10;
                            break;
                        }
                }
            } while (n < 10);

            transform.position = new Vector3(current_plane.Center.x, current_plane.Center.y, -1);
        }
    }

	// Update is called once per frame
	void Update () {
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Player")
        {
            collision.transform.GetComponent<PlayerMove>().AttackedMoveDown(2);
        }

        Destroy(gameObject);
    }
}
