﻿using System;
using UnityEngine;

namespace KYIEngine
{
    public interface ILayoutScope : IDisposable
    {
        Rect GetLastRect();
        Rect GetRect(float size);
        Rect GetRectRatio(float ratio);
        Rect GetRemainingRect();
    }
}