﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace KYIEngine.Animation
{
    /// <summary>
    /// FadeMode
    /// </summary>
    public enum EFadeMode
    {
        /// <summary>
        /// Do nothing
        /// </summary>
        NONE,
        /// <summary>
        /// Fade in
        /// </summary>
        FADE_IN,
        /// <summary>
        /// Fade out
        /// </summary>
        FADE_OUT,
    }

    /// <summary>
    /// Fade mode structure.
    /// </summary>
    [System.Serializable]
    public struct SFadeMode
    {
        /// <summary>
        /// The fade mode
        /// </summary>
        public EFadeMode FadeMode;
        /// <summary>
        /// The duration
        /// </summary>
        public float Duration;
    }
    /// <summary>
    /// Fade in and out the UI component by the given mode queue.
    /// </summary>
    /// <seealso cref="MobileFramework.Animation.ScriptAnimation" />
    /// <seealso cref="MobileFramework.FrameworkMono" />
    [RequireComponent(typeof(Graphic))]
    public class ImageFade : MonoBehaviour
    {
        public bool AnimateOnEnable = true;
        public bool EnableLoop = true;
        /// <summary>
        /// The graphic component
        /// </summary>
        public Graphic[] FadeGroup;

        /// <summary>
        /// The mode queue
        /// </summary>
        public SFadeMode[] ModeQueue;

        public UnityEvent OnAnimationFinished;

        private void OnEnable()
        {
            StartCoroutine(Animate());
        }

        /// <summary>
        /// Animates the graphics component.
        /// </summary>
        /// <returns>
        /// <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/Coroutine.html">The yield instruction</a>.<para />
        /// </returns>
        protected IEnumerator Animate()
        {
            int n = ModeQueue.Length;
            for (int i = 0; i < n; ++i)
            {
                EFadeMode currentMode = ModeQueue[i].FadeMode;
                float duration = ModeQueue[i].Duration;

                float timer = 0.0f;

                while (timer < duration)
                {
                    float alpha = 0.0f;

                    switch (currentMode)
                    {
                        case EFadeMode.FADE_IN:
                            alpha = timer / duration;
                            break;
                        case EFadeMode.FADE_OUT:
                            alpha = 1.0f - (timer / duration);
                            break;
                        default:
                            alpha = FadeGroup[0].color.a;
                            break;
                    }

                    for(int j = 0; j < FadeGroup.Length; j++)
                    {
                        Color nextColor = FadeGroup[j].color;
                        nextColor.a = alpha;

                        FadeGroup[j].color = nextColor;
                    }
                    
                    timer += Time.deltaTime;

                    yield return null;
                }// End While
            }// End For

            if (EnableLoop)
            {
                StartCoroutine(Animate());
            }
            else
            {
                if (OnAnimationFinished != null)
                {
                    OnAnimationFinished.Invoke();
                }
            }
        }
    }
}