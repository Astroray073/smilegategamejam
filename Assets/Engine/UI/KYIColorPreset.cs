﻿// ***********************************************************************
// Assembly         : Assembly-CSharp
// Author           : IN-YEOL, CHOI
// Created          : 10-13-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="ColorPreset.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;

namespace KYIEngine
{
    /// <summary>
    /// Color presets holder
    /// </summary>
    [Modified("1.1", "Name change : ColorPreset -> KYIColorPreset")]
    [Modified("1.1", "Name change : Makes property name starts with capital letter.")]
    public static class KYIColorPreset
    {
        /// <summary>
        /// Gets the pastel red.
        /// </summary>
        /// <value>
        /// The pastel red.
        /// </value>
        public static Color PastelRed { get { return new Color32(255, 105, 97, 200); } }

        /// <summary>
        /// Gets the pastel scarlet.
        /// </summary>
        /// <value>
        /// The pastel scarlet.
        /// </value>
        public static Color PastelScarlet { get { return new Color32(236, 77, 89, 200); } }

        /// <summary>
        /// Gets the pastel blue.
        /// </summary>
        /// <value>
        /// The pastel blue.
        /// </value>
        public static Color PastelBlue { get { return new Color32(128, 162, 255, 200); } }

        /// <summary>
        /// Gets the pastel skyblue.
        /// </summary>
        /// <value>
        /// The pastel skyblue.
        /// </value>
        public static Color PastelSkyblue { get { return new Color32(180, 224, 255, 200); } }

        /// <summary>
        /// Gets the pastel green.
        /// </summary>
        /// <value>
        /// The pastel green.
        /// </value>
        public static Color PastelGreen { get { return new Color32(119, 190, 119, 200); } }

        /// <summary>
        /// Gets the pastel yellow.
        /// </summary>
        /// <value>
        /// The pastel yellow.
        /// </value>
        public static Color PastelYellow { get { return new Color32(253, 253, 150, 200); } }

        /// <summary>
        /// Gets the pastel orange.
        /// </summary>
        /// <value>
        /// The pastel orange.
        /// </value>
        public static Color PastelOrange { get { return new Color32(255, 179, 71, 200); } }

        /// <summary>
        /// Gets the pastel magenta.
        /// </summary>
        /// <value>
        /// The pastel magenta.
        /// </value>
        public static Color PastelMagenta { get { return new Color32(244, 154, 194, 200); } }

        /// <summary>
        /// Gets the pastel purple.
        /// </summary>
        /// <value>
        /// The pastel purple.
        /// </value>
        public static Color PastelPurple { get { return new Color32(150, 70, 150, 200); } }

        /// <summary>
        /// Gets the transparent grey.
        /// </summary>
        /// <value>
        /// The transparent grey.
        /// </value>
        public static Color TransparentGrey { get { return new Color32(120, 120, 120, 120); } }

        /// <summary>
        /// Gets the unity on selected color.
        /// </summary>
        /// <value>The unity on selected color.</value>
        [Modified("1.1.0", "Added")]
        public static Color UnitySelection { get { return new Color32(62, 125, 231, 255); } }

        /// <summary>
        /// Gets the default background color.
        /// </summary>
        /// <value>The default background.</value>
        [Modified("1.1.0", "Added")]
        public static Color DefaultBackground { get { return Color.white; } }
    }
}
