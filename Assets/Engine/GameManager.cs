﻿using UnityEngine;
using System.Collections;

using CP;
public class GameManager : MonoBehaviour
{
    public PlayerMove PlayerA1;
    public PlayerMove PlayerA2;
    public Vector3 SpawnPosition;
    public static GameManager Gamemanager;
    public MapGenerator Map;
    public stone Stone;
    public GameObject UI;

    public GameObject defenderObject;

    // Use this for initialization
    void Start()
    {
        Map.GenerateMap();
        Gamemanager = this;
        PlayerA1 = GameObject.Instantiate<PlayerMove>(PlayerA1);
        SpawnPosition = Map.PlaneOnFloor[0][0].Center;
        SpawnPosition.z = -1;
        PlayerA1.transform.position = SpawnPosition;
        PlayerA1.gameManager = this;
        PlayerA1.playerNumber = 0;

        PlayerA2 = GameObject.Instantiate<PlayerMove>(PlayerA2);
        SpawnPosition = Map.PlaneOnFloor[0][Map.PlaneOnFloor.Count*2 -2].Center;
        SpawnPosition.z = -1;
        PlayerA2.transform.position = SpawnPosition;
        PlayerA2.gameManager = this;
        PlayerA2.playerNumber = 1;

        CP.Plane topPlane = Map.PlaneOnFloor[Map.PlaneOnFloor.Count - 1][0];
        defenderObject.transform.position = new Vector3(topPlane.Center.x, topPlane.Center.y, defenderObject.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
