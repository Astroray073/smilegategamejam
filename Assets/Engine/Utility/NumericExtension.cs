﻿using System;
using System.Reflection;
using System.ComponentModel;
using UnityEngine;

/// <summary>
/// Collections of value type extensions.
/// </summary>
public static class NumericExtension
{
    #region INTEGER
    /// <summary>
    /// Get absolute value the specified value.
    /// </summary>
    /// <param name="i">The value.</param>
    /// <returns>The absolute value.</returns>
    public static int Abs(this int i)
    {
        if (i < 0)
        {
            return -i;
        }
        else
        {
            return i;
        }
    }
    /// <summary>
    /// Get the sign of the specified value.
    /// </summary>
    /// <param name="i">The value.</param>
    /// <returns>The sign of the value.</returns>
    public static int Sign(this int i)
    {
        if (i < 0)
        {
            return -1;
        }
        else if (i > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    #endregion

    #region FLOAT
    /// <summary>
    /// Get absolute value the specified value.
    /// </summary>
    /// <param name="f">The value.</param>
    /// <returns>The absolute value.</returns>
    public static float Abs(this float f)
    {
        if (f < 0)
        {
            return -f;
        }
        else
        {
            return f;
        }
    }
    /// <summary>
    /// Get the sign of the specified value.
    /// </summary>
    /// <param name="f">The value.</param>
    /// <returns>The sign of the value.</returns>
    public static float Sign(this float f)
    {
        if (f < 0)
        {
            return -1.0f;
        }
        else if (f > 0)
        {
            return 1.0f;
        }
        else
        {
            return 0.0f;
        }
    }
    #endregion

    #region Enum
    /// <summary>
    /// Gets the description attribute value string.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns>Defined user-friendly name.</returns>
    public static string GetDescription(this Enum value)
    {
        Type type = value.GetType();
        string name = Enum.GetName(type, value);
        if (name != null)
        {
            FieldInfo field = type.GetField(name);
            if (field != null)
            {
                DescriptionAttribute attr =
                       Attribute.GetCustomAttribute(field,
                         typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attr != null)
                {
                    return attr.Description;
                }
            }
        }
        return null;
    }
    /// <summary>
    /// Determines whether the specified enum has flag.
    /// </summary>
    /// <typeparam name="T">The type of enum.</typeparam>
    /// <param name="value">The value.</param>
    /// <param name="other">The other enum to compare.</param>
    /// <returns>
    /// <c>true</c> if the value has flag otherwise <c>false</c>.
    /// </returns>
    public static bool HasFlag<T>(this T value, T other) where T : struct, IConvertible
    {
        var type = value.GetType();
        var attrs = type.GetCustomAttributes(typeof(FlagsAttribute), false);

        if (!value.GetType().IsEnum)
        {
            Debug.LogAssertionFormat("{0} must be enum with flags attribute", value.ToString());
            return false;
        }

        bool hasFlagAttr = false;
        for (int i = 0; i < attrs.Length; i++)
        {
            if (attrs[i].GetType().Equals(typeof(FlagsAttribute)))
            {
                hasFlagAttr = true;
            }
        }

        if (!hasFlagAttr)
        {
            Debug.LogErrorFormat("{0} is not flag.", value.ToString());
        }

        return (value.ToInt32(null) & other.ToInt32(null)) != 0;
    }
    #endregion

    #region Vector3
    /// <summary>
    /// Rotates the vector based on the euler angles.
    /// </summary>
    /// <param name="vector">The vector.</param>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="z">The z.</param>
    /// <returns></returns>
    public static Vector3 Rotate(this Vector3 vector, float x, float y, float z)
    {
        return Quaternion.Euler(x, y, z) * vector;
    }
    /// <summary>
    /// Rotates the vector based on the euler angles.
    /// </summary>
    /// <param name="vector">The vector.</param>
    /// <param name="x">The x.</param>
    /// <param name="y">The y.</param>
    /// <param name="z">The z.</param>
    /// <returns></returns>
    public static Vector3 Rotate(this Vector3 vector, Vector3 eulerAngles)
    {
        return Quaternion.Euler(eulerAngles) * vector;
    }
    #endregion
}

