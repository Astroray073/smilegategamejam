﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using CP;
public class PlayerMove : MonoBehaviour
{
    public Vector3 WhereMove;
    public Vector3 JumpMove;

    public float MoveTime;
    public float MoveDelay;

    public float UpTime = 12f;
    public float UpDelay;

    public int MaxFloor;
    public int currentFloor;

    public float StateTime;
    public bool AttackedState;

    public bool MoveFlag;
    public bool MoveUp;
    public bool UpDown;
    public bool Reverse;

    public GameManager gameManager;

    public CP.Plane currentPlane;
    public CP.Plane PresentPlane;

    public int playerNumber;
    public static string[] KeySet = { "Horizontal", "Vertical", "Horizontal2", "Vertical2" };

    // Use this for initialization
    void Start()
    {
        WhereMove.z = 0;
        MoveFlag = true;
        MoveUp = false;
        Reverse = false;
        MaxFloor = 0;
        currentFloor = 0;
        JumpMove.z = 0;
        UpDelay = 0;
        currentPlane = gameManager.Map.GetPlane(transform.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (MoveFlag)
        {
            if (Input.GetButton(KeySet[playerNumber * 2]))
            {
                PresentPlane = currentPlane;
                if (Input.GetAxis(KeySet[playerNumber * 2]) > 0)
                {
                    if (Reverse && currentPlane.LeftPlane != null)
                    {
                        currentPlane = currentPlane.LeftPlane;
                        MoveDelay = MoveTime;
                        MoveFlag = false;
                        UpDown = false;
                    }
                    else if (currentPlane.RightPlane != null)
                    {
                        currentPlane = currentPlane.RightPlane;
                        MoveDelay = MoveTime;
                        MoveFlag = false;
                        UpDown = false;
                    }
                    else
                    {
                        MoveDelay = 0;
                        MoveFlag = true;
                    }
                    WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
                    WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
                }
                else if (Input.GetAxis(KeySet[playerNumber * 2]) < 0)
                {
                    if (Reverse && currentPlane.RightPlane != null)
                    {
                        currentPlane = currentPlane.RightPlane;
                        MoveDelay = MoveTime;
                        MoveFlag = false;
                        UpDown = false;
                    }
                    else if (currentPlane.LeftPlane != null)
                    {
                        currentPlane = currentPlane.LeftPlane;
                        MoveDelay = MoveTime;
                        MoveFlag = false;
                        UpDown = false;
                    }
                    else
                    {
                        MoveDelay = 0;
                        MoveFlag = true;
                    }
                    WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
                    WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
                }
            }

            else if (Input.GetButton(KeySet[playerNumber * 2 + 1]))
            {
                PresentPlane = currentPlane;
                if (Input.GetAxis(KeySet[playerNumber * 2 + 1]) > 0)
                {

                    if (currentFloor < MaxFloor)
                        MoveUp = true;
                    else if (UpDelay >= UpTime)
                    {
                        UpDelay = 0;
                        MoveUp = true;
                    }
                    if (MoveUp)
                    {
                        if (Reverse && currentPlane.BottomPlane != null)
                        {
                            currentPlane = currentPlane.BottomPlane;
                            MoveFlag = false;
                            UpDown = true;
                            currentFloor--;
                        }
                        else
                        {
                            currentPlane = currentPlane.UpPlane;
                            MoveFlag = false;
                            currentFloor++;
                            MaxFloor = Mathf.Max(currentFloor, MaxFloor);
                            UpDown = true;
                        }
                        WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
                        WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
                        MoveDelay = MoveTime;
                    }
                }

                else if (Input.GetAxis(KeySet[playerNumber * 2 + 1]) < 0)
                {
                    if (Reverse)
                    {
                        if (currentFloor < MaxFloor)
                            MoveUp = true;
                        else if (UpDelay >= UpTime)
                        {
                            UpDelay = 0;
                            MoveUp = true;
                        }
                        if (MoveUp)
                        {
                            currentPlane = currentPlane.UpPlane;

                            WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
                            WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
                            MoveDelay = MoveTime;
                            MoveFlag = false;
                            UpDown = true;
                            currentFloor++;
                            MaxFloor = Mathf.Max(currentFloor, MaxFloor);
                        }
                    }
                    else if (currentPlane.BottomPlane != null)
                    {
                        currentPlane = currentPlane.BottomPlane;

                        WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
                        WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
                        MoveDelay = MoveTime;
                        currentFloor--;
                        MoveFlag = false;
                        UpDown = true;
                    }
                }
            }
        }

        else
        {
            if (MoveDelay > 0)
            {
                Move();
                MoveDelay -= Time.fixedDeltaTime;
            }

            else
            {
                transform.position = currentPlane.Center;
                transform.position += new Vector3(0, 0, -1);
                MoveFlag = true;
                if (AttackedState)
                    AttackedState = !AttackedState;
                if (MoveUp)
                    MoveUp = false;
            }
        }

        if (UpDelay < UpTime)
        {
            UpDelay += Time.fixedDeltaTime;
        }

        if (StateTime > 0)
        {
            StateTime -= Time.fixedDeltaTime;
        }
        else if (Reverse)
        {
            Reverse = false;
        }

        // SetUI
        Text text = GameManager.Gamemanager.UI.transform.FindChild("Player/P" + (playerNumber + 1).ToString() + "/Max Floor").GetComponent<Text>();
        text.text = (MaxFloor + 1).ToString();

        Slider jumpSlider = GameManager.Gamemanager.UI.transform.FindChild("Player/P" + (playerNumber + 1).ToString() + "/Jump Slider").GetComponent<Slider>();
        jumpSlider.value = (UpDelay / UpTime) * 100;
    }

    public void MoveRevers(float time)
    {
        if (!AttackedState)
        {
            Reverse = !Reverse;
            StateTime = time;
            AttackedState = true;
        }
    }

    public void DontMovd(int n)
    {
        if (!AttackedState)
        {
            if (!MoveFlag)
            {
                currentPlane = PresentPlane;
                transform.position = new Vector3(currentPlane.Center.x, currentPlane.Center.y, transform.position.z);
                if (MoveUp)
                {
                    currentFloor--;
                    MoveUp = false;
                }
            }
            MoveDelay = MoveTime * n;
            MoveFlag = false;
            WhereMove = Vector3.zero;

            AttackedState = true;
        }
    }

    public void AttackedMoveDown(int n)
    {
        if (!AttackedState)
        {
            if (!MoveFlag)
            {
                currentPlane = PresentPlane;
                if (MoveUp)
                {
                    currentFloor--;
                    MoveUp = false;
                }
            }

            int k = n;
            MoveFlag = false;
            while (currentFloor >= 0)
            {
                if (currentPlane.BottomPlane == null)
                    break;
                currentPlane = currentPlane.BottomPlane;
                currentFloor--;
            }
            WhereMove.x = (currentPlane.Center.x - transform.position.x) * Time.deltaTime / MoveTime;
            WhereMove.y = (currentPlane.Center.y - transform.position.y) * Time.deltaTime / MoveTime;
            MoveDelay = MoveTime;
            MoveFlag = false;

            MoveUp = false;
            AttackedState = true;
        }
    }

    void Move()
    {
        if (UpDown)
        {
            if (MoveUp)
            {
                JumpMove.x = WhereMove.x * (MoveTime / 2 - MoveDelay) * 2f;
            }
            else
            {
                JumpMove.x = -WhereMove.x * (MoveTime / 2 - MoveDelay) * 2f;
            }
            JumpMove.y = -Mathf.Abs(WhereMove.y) * (MoveTime / 2 - MoveDelay) * 2f;
        }
        else
        {
            JumpMove.x = Mathf.Abs(WhereMove.y) * (MoveTime / 2 - MoveDelay) * 4f;
            JumpMove.y = -Mathf.Abs(WhereMove.x) * (MoveTime / 2 - MoveDelay) * 4f;
        }
        transform.position += WhereMove + JumpMove;

    }
}
