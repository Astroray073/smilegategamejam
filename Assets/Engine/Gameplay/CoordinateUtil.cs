﻿using UnityEngine;
using System.Collections;

namespace CP
{
    public static class CoordinateUtil
    {
        public static float TileHalfWidth { get { return _tileHalfWidth; } }
        private static readonly float _tileHalfWidth = 0.39f;
        private static readonly float _inverseHalfWidth = 1 / _tileHalfWidth;
        public static Vector2 CoordinateToWorldPoint(CleoPoint coordinatePoint)
        {
            return CoordinateToWorldPoint(coordinatePoint.x, coordinatePoint.y);
        }
        public static Vector2 CoordinateToWorldPoint(int i, int j)
        {
            return new Vector2(_tileHalfWidth * (i + j), 0.5f * _tileHalfWidth * (j - i));
        }
        public static Vector3 CoordinateToWorldPoint(int i, int j, float fixedDepth)
        {
            var pos = CoordinateToWorldPoint(i, j);
            Vector3 ret = pos;
            ret.z = fixedDepth;
            return ret;
        }

        public static CleoPoint WorldPointToCoordinate(Vector2 point)
        {
            int i = (int)(0.5f * _inverseHalfWidth * (point.x - 2.0f * point.y));
            int j = (int)(0.5f * _inverseHalfWidth * (point.x + 2.0f * point.y));
            return new CleoPoint(i, j);
        }
    }
}