﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CP
{
    public class SpawnableTileMarker : MonoBehaviour
    {
        [SerializeField, KYIEngine.Increment(0.1f)]
        private float _gizmoSize;
        [SerializeField]
        private Color _gizmoColor;

        public List<Vector3> SpawnablePoints
        {
            get
            {
                return _spawnablePoints;
            }
        }
        private List<Vector3> _spawnablePoints = new List<Vector3>();
        private Queue<Plane> _testingPlanes = new Queue<Plane>();
        public void UpdateSpawnablePoints()
        {
            SpawnablePoints.Clear();
            _testingPlanes.Clear();
            _testingPlanes.Enqueue(MapGenerator.Instance.GetPlane(transform.position));
            UpdatePoints();
        }
        private void UpdatePoints()
        {
            for (int i = 0; i < 5; i++)
            {
                TestPlane(_testingPlanes.Dequeue());
            }
        }
        private void TestPlane(Plane plane)
        {
            if (plane == null)
            {
                return;
            }

            for (int i = 0; i < 4; i++)
            {
                if (plane[i] != null
                    && plane[i] != MapGenerator.Instance.GetPlane(transform.position))
                {
                    var center = plane[i].Center;

                    if (!SpawnablePoints.Contains(center))
                    {
                        SpawnablePoints.Add(center);

                        if (!_testingPlanes.Contains(plane[i]))
                        {
                            _testingPlanes.Enqueue(plane[i]);
                        }
                    }
                }
            }
        }
        public void OnDrawGizmosSelected()
        {
            Gizmos.color = _gizmoColor;
            for (int i = 0; i < SpawnablePoints.Count; i++)
            {
                Gizmos.DrawWireSphere(SpawnablePoints[i], _gizmoSize);
            }
        }
    }
}