﻿using UnityEngine;
using System.Collections;

namespace CP
{
    public class TestObject : MonoBehaviour
    {
        [ContextMenu("Test")]
        public void Test()
        {
            Vector2 v = new Vector2(2.3f, 3.3f);
            var c = CoordinateUtil.WorldPointToCoordinate(v);

            Debug.Log(v);
            Debug.Log(c);

            Debug.Log(CoordinateUtil.CoordinateToWorldPoint(c));
        }
    }
}