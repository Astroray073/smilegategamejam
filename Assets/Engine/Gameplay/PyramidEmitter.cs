﻿using UnityEngine;
using System.Collections;
using KYIEngine;

namespace CP
{
    [RequireComponent(typeof(MapGenerator))]
    public class PyramidEmitter : MonoBehaviour
    {
        [Header("Emitter Settings"), SerializeField, Increment(1.0f, true)]
        private float _minEmitterForce = -20.0f;
        [SerializeField, Increment(1.0f, true)]
        private float _maxEmitterForce = 20.0f;
        [SerializeField, Increment(1.0f, true)]
        private int _limitFloorCount;
        [Header("Jittering"), SerializeField, Increment(0.1f, true)]
        private float _jitteringTime = 1.0f;
        [SerializeField, Increment(0.1f, true)]
        private float _jitteringAmount = 0.5f;
        [SerializeField, Increment(0.1f, true)]
        private float _jitteringRepeatTime = 0.1f;
        [SerializeField]
        private float[] _emitterTiming;

        public void StartEmitterSequence()
        {
            if (_emitterTiming.Length > 0)
            {
                StartCoroutine(EmitPryamidFloor());
            }
        }

        private IEnumerator EmitPryamidFloor()
        {
            int floorIndex = 0;
            while (floorIndex < MapGenerator.Instance.PlaneTileOnFloor.Count - _limitFloorCount)
            {
                yield return new WaitForSeconds(_emitterTiming[floorIndex] - _jitteringTime * 0.9f);
                yield return StartCoroutine(JitteringFloor(floorIndex));

                Debug.LogFormat("Emit floor {0}", floorIndex);
                EmitFloor(floorIndex);
                floorIndex++;
            }
        }

        private void EmitFloor(int floorIndex)
        {
            if (MapGenerator.Instance.PlaneOnFloor.Count > 0)
            {
                var currentFloor = MapGenerator.Instance.PlaneTileOnFloor[floorIndex];
                for (int i = 0; i < currentFloor.Length; i++)
                {
                    var tileRigidBody = currentFloor[i].GetComponent<Rigidbody2D>();
                    tileRigidBody.gravityScale = 1.0f;
                    tileRigidBody.AddForce(GetRandomImpulseVector());
                }
            }
        }

        private Vector2 GetRandomImpulseVector()
        {
            return new Vector2(Random.Range(_minEmitterForce, _maxEmitterForce),
                               Random.Range(_minEmitterForce, _maxEmitterForce));
        }

        private IEnumerator JitteringFloor(int floorIndex)
        {
            float startTime = Time.timeSinceLevelLoad;
            while (Time.timeSinceLevelLoad - startTime < _jitteringTime)
            {
                Jittering(floorIndex);
                yield return new WaitForSeconds(_jitteringRepeatTime);
            }
        }

        private bool _changeJitter = false;
        private void Jittering(int floorIndex)
        {
            if (MapGenerator.Instance.PlaneOnFloor.Count > 0)
            {
                var currentFloor = MapGenerator.Instance.PlaneTileOnFloor[floorIndex];
                for (int i = 0; i < currentFloor.Length; i++)
                {
                    var tileTransform = currentFloor[i].transform;
                    if (_changeJitter)
                    {
                        tileTransform.rotation = Quaternion.Euler(0.0f, 0.0f, -_jitteringAmount);
                    }
                    else
                    {
                        tileTransform.rotation = Quaternion.Euler(0.0f, 0.0f, _jitteringAmount);
                    }
                    _changeJitter = !_changeJitter;
                }
            }
        }

    }
}