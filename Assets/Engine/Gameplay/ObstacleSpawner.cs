﻿using UnityEngine;
using System.Collections;
using KYIEngine.DesignPattern;
using System;
using System.Collections.Generic;

namespace CP
{
    [RequireComponent(typeof(SpawnableTileMarker))]
    public class ObstacleSpawner : MonoBehaviour
    {
        [SerializeField]
        private Block _blockPrefab;
        [SerializeField]
        private Trap _trapPreafab;

        [SerializeField, KYIEngine.Increment(1.0f)]
        private int _objectCount;
        [SerializeField]
        private float _depth = -1.0f;
        [SerializeField, KYIEngine.Increment(1.0f)]
        private float _repeatTime = 10.0f;

        private SpawnableTileMarker _tileMarker;

        private Block[] _blockInstances;
        private Trap[] _trapInstances;

        private void Awake()
        {
            _tileMarker = GetComponent<SpawnableTileMarker>();
            _blockInstances = new Block[2];
            _trapInstances = new Trap[2];
            for (int i = 0; i < _objectCount; i++)
            {
                _blockInstances[i] = GameObject.Instantiate<Block>(_blockPrefab);
                _trapInstances[i] = GameObject.Instantiate<Trap>(_trapPreafab);

                _blockInstances[i].gameObject.SetActive(false);
                _trapInstances[i].gameObject.SetActive(false);
            }
        }

        private void Start()
        {
            InvokeRepeating("Spawn", 0.0f, _repeatTime);
        }
        
        private void ResetObjects()
        {
            for (int i = 0; i < _objectCount; i++)
            {
                _blockInstances[i].gameObject.SetActive(false);
                _trapInstances[i].gameObject.SetActive(false);
            }
        }

        public void Spawn()
        {
            _tileMarker.UpdateSpawnablePoints();
            
            // Block
            for (int i = 0; i < _objectCount; i++)
            {
                var block = _blockInstances[i];
                block.gameObject.SetActive(true);

                var picked = UnityEngine.Random.Range(0, _tileMarker.SpawnablePoints.Count);
                var pos = _tileMarker.SpawnablePoints[picked];
                pos.z = _depth;
                block.transform.position = pos;

                _tileMarker.SpawnablePoints.RemoveAt(picked);
            }

            // Trap
            for (int i = 0; i < _objectCount; i++)
            {
                var trap = _trapInstances[i];
                trap.gameObject.SetActive(true);

                var picked = UnityEngine.Random.Range(0, _tileMarker.SpawnablePoints.Count);
                var pos = _tileMarker.SpawnablePoints[picked];
                pos.z = _depth;
                trap.transform.position = pos;

                _tileMarker.SpawnablePoints.RemoveAt(picked);
            }
        }
    }
}