﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CP
{
    public struct CleoPoint
    {
        public int x;
        public int y;

        public CleoPoint(int indexX, int indexY)
        {
            x = indexX;
            y = indexY;
        }
        
        public override string ToString()
        {
            return string.Format("({0}, {1})", x, y);
        }
        public bool Equals(CleoPoint other)
        {
            return (x == other.x && y == other.y);
        }
    }

    public class Plane
    {
        public CleoPoint[] PlanePoints { get { return _planePoints; } }
        private CleoPoint[] _planePoints = new CleoPoint[4];
        public Plane LeftPlane;
        public Plane RightPlane;
        public Plane UpPlane;
        public Plane BottomPlane;

        public Vector2 Center
        {
            get
            {
                var vectorSum = Vector2.zero;
                for (int i = 0; i < _planePoints.Length; i++)
                {
                    vectorSum += CoordinateUtil.CoordinateToWorldPoint(_planePoints[i]);
                }
                return vectorSum / 4.0f;
            }
        }
        public float MaxX
        {
            get
            {
                return CoordinateUtil.CoordinateToWorldPoint(_planePoints[3]).x;
            }
        }
        public float MinX
        {
            get
            {
                return CoordinateUtil.CoordinateToWorldPoint(_planePoints[1]).x;
            }
        }
        public float MaxY
        {
            get
            {
                return CoordinateUtil.CoordinateToWorldPoint(_planePoints[2]).y;
            }
        }
        public float MinY
        {
            get
            {
                return CoordinateUtil.CoordinateToWorldPoint(_planePoints[0]).y;
            }
        }

        public CleoPoint PivotPoint
        {
            get
            {
                return _planePoints[0];
            }
        }

        public Plane(int i, int j)
        {
            _planePoints[0] = new CleoPoint() { x = i, y = j };
            _planePoints[1] = new CleoPoint() { x = i - 1, y = j };
            _planePoints[2] = new CleoPoint() { x = i - 1, y = j + 1 };
            _planePoints[3] = new CleoPoint() { x = i, y = j + 1 };
        }
        
        public bool Contains(Vector2 worldPoint)
        {
            for (int i = 0; i < _planePoints.Length; i++)
            {
                Vector2 edge;
                Vector2 testTarget;
                if (i != _planePoints.Length - 1)
                {
                    edge = CoordinateUtil.CoordinateToWorldPoint(_planePoints[i + 1]) - CoordinateUtil.CoordinateToWorldPoint(_planePoints[i]);
                    testTarget = worldPoint - CoordinateUtil.CoordinateToWorldPoint(_planePoints[i]);
                }
                else
                {
                    edge = CoordinateUtil.CoordinateToWorldPoint(_planePoints[0]) - CoordinateUtil.CoordinateToWorldPoint(_planePoints[i]);
                    testTarget = worldPoint - CoordinateUtil.CoordinateToWorldPoint(_planePoints[i]);
                }

                if (Vector3.Cross(edge, testTarget).z > 0.0f)
                {
                    return false;
                } 
            }

            return true;
        }

        // Iterate left, up, right, bottom
        public Plane this[int i]
        {
            get
            {
                if (i == 0)
                {
                    return LeftPlane;
                }
                else if (i == 1)
                {
                    return UpPlane;
                }
                else if (i == 2)
                {
                    return RightPlane;
                }
                else if (i == 3)
                {
                    return BottomPlane;
                }
                else
                {
                    throw new System.ArgumentException();
                }
            }
        }
    }
}

