﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using KYIEngine.DesignPattern;

namespace CP
{
    public class MapGenerator : StaticMonoBehaviour<MapGenerator>
    {
        #region Exposed Fields        
        public List<Plane[]> PlaneOnFloor = new List<Plane[]>();
        public List<GameObject[]> PlaneTileOnFloor = new List<GameObject[]>();
        #endregion

        #region Private Fields
        [SerializeField]
        private bool _generateMapOnEnable = true;
        [SerializeField]
        private GameObject _tilePrefab;
        [SerializeField, Range(0, 10)]
        private int _floorCount;
        [SerializeField, HideInInspector]
        private Transform _mapGroup;
        [SerializeField]
        private UnityEvent _onMapGenerated;
        private const float _depthAdjustAmount = 0.01f;
        #endregion

        #region Unity Message
        private void Start()
        {
            if (_generateMapOnEnable)
            {
                GenerateMap();
            }
        }
        #endregion

        #region Public methods
        [ContextMenu("Generate")]
        public void GenerateMap()
        {
            ClearMap();

            if (!_mapGroup)
            {
                _mapGroup = new GameObject("Map Group").transform;
            }

            int indexX = 0;
            int indexY = 0;

            for (int i = _floorCount - 1; i >= 0; i--)
            {
                var mapBlockPerCount = 2 * i + 1;
                float depthAdjust = 0.0f;

                PlaneOnFloor.Add(new Plane[mapBlockPerCount]);
                PlaneTileOnFloor.Add(new GameObject[mapBlockPerCount]);

                for (int j = 0; j < mapBlockPerCount; j++)
                {
                    PlaneTileOnFloor[_floorCount - i - 1][j] = InstantiateTilePrefab(indexX, indexY, depthAdjust);
                    PlaneOnFloor[_floorCount - i - 1][j] = new Plane(indexX, indexY);

                    if (j < i)
                    {
                        indexX++;
                        depthAdjust -= _depthAdjustAmount;
                    }
                    else
                    {
                        indexY++;
                        depthAdjust += _depthAdjustAmount;
                    }
                }

                indexX -= (i + 1);
                indexY -= (i - 1);
            }

            LinkPlaneMap();

            if (_onMapGenerated != null)
            {
                _onMapGenerated.Invoke();
            }
        }
        [ContextMenu("Clear")]
        public void ClearMap()
        {
            if (_mapGroup)
            {
                DestroyImmediate(_mapGroup.gameObject);
            }
        }
        public Plane GetPlane(Vector2 worldPoint)
        {
            for (int i = 0; i < PlaneOnFloor.Count; i++)
            {
                var currentPlaneOnFloor = PlaneOnFloor[i];
                for (int j = 0; j < currentPlaneOnFloor.Length; j++)
                {
                    if (currentPlaneOnFloor[j].Contains(worldPoint))
                    {
                        return currentPlaneOnFloor[j];
                    }
                }
            }
            return null;
        }
        public int[] GetPlaneID(Plane plane)
        {
            for (int i = 0; i < PlaneOnFloor.Count; i++)
            {
                var currentPlaneOnFloor = PlaneOnFloor[i];
                for (int j = 0; j < currentPlaneOnFloor.Length; j++)
                {
                    if (currentPlaneOnFloor[j] == plane)
                    {
                        return new int[2] { i, j };
                    }
                }
            }
            return null;
        }
        #endregion

        #region Private Methods        
        private void LinkPlaneMap()
        {
            for (int i = 0; i < PlaneOnFloor.Count; i++)
            {
                var currentFloorPlane = PlaneOnFloor[i];
                int midIndex = currentFloorPlane.Length / 2;
                for (int j = 0; j < currentFloorPlane.Length; j++)
                {
                    if (j < midIndex)
                    {
                        // Util Mid
                        if (j != 0)
                        {
                            currentFloorPlane[j].LeftPlane = currentFloorPlane[j - 1];
                        }

                        currentFloorPlane[j].UpPlane = PlaneOnFloor[i + 1][j];
                        currentFloorPlane[j].RightPlane = currentFloorPlane[j + 1];

                        if (i > 0)
                        {
                            currentFloorPlane[j].BottomPlane = PlaneOnFloor[i - 1][j];
                        }
                    }
                    else if (j == midIndex)
                    {
                        // Mid index
                        if (i < PlaneOnFloor.Count - 1)
                        {
                            currentFloorPlane[j].LeftPlane = currentFloorPlane[j - 1];
                            currentFloorPlane[j].RightPlane = currentFloorPlane[j + 1];
                            currentFloorPlane[j].UpPlane = PlaneOnFloor[i + 1][j - 1];

                            if (i != 0)
                            {
                                currentFloorPlane[j].BottomPlane = PlaneOnFloor[i - 1][j + 1];
                            }
                        }
                    }
                    else
                    {
                        // Mid to end
                        if (j != currentFloorPlane.Length - 1)
                        {
                            currentFloorPlane[j].RightPlane = currentFloorPlane[j + 1];
                        }

                        currentFloorPlane[j].UpPlane = PlaneOnFloor[i + 1][j - 2];
                        currentFloorPlane[j].LeftPlane = currentFloorPlane[j - 1];

                        if (i > 0)
                        {
                            currentFloorPlane[j].BottomPlane = PlaneOnFloor[i - 1][j + 2];
                        }
                    }
                }

                if (i == PlaneOnFloor.Count - 1)
                {
                    currentFloorPlane[0].BottomPlane = PlaneOnFloor[PlaneOnFloor.Count - 2][1];
                }
            }
        }
        private GameObject InstantiateTilePrefab(int indexX, int indexY, float depthAdjust)
        {
            var go = GameObject.Instantiate(_tilePrefab);
            go.name = string.Format("MapBlock({0},{1})", indexX, indexY);
            go.transform.position = CoordinateUtil.CoordinateToWorldPoint(indexX, indexY, depthAdjust);
            go.transform.SetParent(_mapGroup);
            go.GetComponent<Rigidbody2D>().gravityScale = 0.0f;
            return go;
        }
        #endregion
    }
}