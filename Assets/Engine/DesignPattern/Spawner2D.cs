﻿using System.Collections.Generic;
using UnityEngine;

namespace KYIEngine.DesignPattern
{
    /// <summary>
    /// The mode what method to use for spawning.
    /// </summary>
    public enum ESpawnMode
    {
        SM_SQUARE,
        SM_CIRCLE,
        SM_POINT
    }

    /// <summary>
    /// The class has basic spawning functionality in 2D space.
    /// </summary>
    /// <typeparam name="T">The type of object to spawn.</typeparam>
    /// <seealso cref="MobileFramework.ObjectPool{T}" />
    /// <seealso cref="MobileFramework.ISpawner" />
    public abstract class Spawner2D<T> : ObjectPool<T> where T : Component
    {
        /// <summary>
        /// The state whether this spawns objects.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is spawning; otherwise, <c>false</c>.
        /// </value>
        public bool IsSpawning { get; protected set; }

        /// <summary>
        /// Gets the current spawned point.
        /// </summary>
        /// <value>
        /// The current spawned point.
        /// </value>
        public Transform CurrentSpawnedPoint { get; private set; }


        #region Editor Settings
        /// <summary>
        /// The spawn mode
        /// </summary>
        public ESpawnMode SpawnMode;

        /// <summary>
        /// Starts to spawn when it is enabled.
        /// </summary>
        public bool ForceEnableStartSpawn = false;

        /// <summary>
        /// Enables spawning objects by running spawning method continuously.
        /// </summary>
        public bool EnableLooping = false;

        /// <summary>
        /// Allows the spawner to spawn objects when the number of active objects reaches the <see cref="ObjectPool{T}.MaxObjectCount"/>
        /// </summary>
        public bool AllowSpawnMore = false;

        /// <summary>
        /// The delay time when <see cref="StartSpawn"/> is called.
        /// </summary>
        public float StartTime;

        /// <summary>
        /// The interval time when spawning objects continuously if <see cref="EnableLooping"/> is <c>true</c>
        /// </summary>
        public float IntervalTime;

        /// <summary>
        /// The spawn inner size for <see cref="ESpawnMode.SM_SQUARE"/> and <see cref="ESpawnMode.SM_SQUARE"/> modes.
        /// </summary>
        public float SpawnInnerSize;

        /// <summary>
        /// The spawn outer size for <see cref="ESpawnMode.SM_SQUARE"/> and <see cref="ESpawnMode.SM_SQUARE"/> modes.
        /// </summary>
        public float SpawnOuterSize;

        /// <summary>
        /// The inner color when the gizmo is drawn.
        /// </summary>
        public Color InnerGizmoColor = Color.red;

        /// <summary>
        /// The outer color when the gizmo is drawn.
        /// </summary>
        public Color OuterGizmoColor = Color.green;

        /// <summary>
        /// The spawn points for <see cref="ESpawnMode.SM_POINT"/>
        /// </summary>
        public List<Transform> SpawnPoints = new List<Transform>();
        #endregion

        #region ISpawner
        /// <summary>
        /// [<see cref="ISpawner"/>] Starts the spawning.
        /// </summary>
        public void StartSpawn()
        {
            if (!IsInitialized)
            {
                Debug.LogError("The spawner is not initialized.");
                return;
            }

            if (IsSpawning)
            {
                Debug.Log("This spawner is already activated.");
            }
            else
            {
                Debug.Log("Start Spawn.");
                IsSpawning = true;
            }

            if (SpawnMode == ESpawnMode.SM_CIRCLE)
            {
                if (EnableLooping)
                {
                    InvokeRepeating("CircularSpawn", StartTime, IntervalTime);
                }
                else
                {
                    Invoke("CircularSpawn", StartTime);
                }
            }
            else if (SpawnMode == ESpawnMode.SM_SQUARE)
            {
                if (EnableLooping)
                {
                    InvokeRepeating("SqaureSpawn", StartTime, IntervalTime);
                }
                else
                {
                    Invoke("SqaureSpawn", StartTime);
                }
            }
            else if (SpawnMode == ESpawnMode.SM_POINT)
            {
                if (EnableLooping)
                {
                    InvokeRepeating("PointSpawn", StartTime, IntervalTime);
                }
                else
                {
                    Invoke("PointSpawn", StartTime);
                }
            }
        }
        /// <summary>
        /// [<see cref="ISpawner"/>] Stops the spawning.
        /// </summary>
        public void StopSpawn()
        {
            CancelInvoke();
            StopAllCoroutines();
            IsSpawning = false;
        }
        #endregion

        /// <summary>
        /// Unity message <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.Awake.html">Awake</a>.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();

            if (IsInitialized && ForceEnableStartSpawn)
            {
                StartSpawn();
            }
            else if (!IsInitialized && ForceEnableStartSpawn)
            {
                InitializePool();
                StartSpawn();
            }
        }

        /// <summary>
        /// Unity message <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.OnDrawGizmos.html">OnDrawGizmos</a>.
        /// </summary>
        protected virtual void OnDrawGizmos()
        {
            if (SpawnMode == ESpawnMode.SM_CIRCLE)
            {
                Gizmos.color = InnerGizmoColor;
                Gizmos.DrawWireSphere(transform.position, SpawnInnerSize);
                Gizmos.color = OuterGizmoColor;
                Gizmos.DrawWireSphere(transform.position, SpawnOuterSize);
            }
            else if (SpawnMode == ESpawnMode.SM_SQUARE)
            {
                Gizmos.color = InnerGizmoColor;
                Gizmos.DrawWireCube(transform.position, 2.0f * new Vector3(SpawnInnerSize, SpawnInnerSize));
                Gizmos.color = OuterGizmoColor;
                Gizmos.DrawWireCube(transform.position, 2.0f * new Vector3(SpawnOuterSize, SpawnOuterSize));
            }
            else if (SpawnMode == ESpawnMode.SM_POINT)
            {
                Gizmos.color = OuterGizmoColor;
                for (int i = 0; i < SpawnPoints.Count; i++)
                {
                    if (SpawnPoints[i] != null && SpawnPoints[i].gameObject.activeInHierarchy)
                    {
                        Gizmos.DrawSphere(SpawnPoints[i].position, 0.2f);
                    }
                }
            }
        }

        #region Public Methods
        /// <summary>
        /// Resets the position of the spawner.
        /// </summary>
        public void ResetPosition()
        {
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.identity;
        }

        /// <summary>
        /// Resets the local position of the spawner
        /// </summary>
        public void ResetLocalPosition()
        {
            transform.localPosition = new Vector3(0.0f, 0.0f, 10.0f);
            transform.localRotation = Quaternion.identity;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Randomly spawns A object each time when the method is called
        /// base on the <see cref="SpawnInnerSize"/> and <see cref="SpawnOuterSize"/> of a circle.
        /// </summary>
        protected virtual void CircularSpawn()
        {
            if (!AllowSpawnMore && _spawningCount >= MaxObjectCount)
            {
                return;
            }

            float spawnDistance = Random.Range(SpawnInnerSize, SpawnOuterSize);
            float spawnAngle = Random.Range(0, 360.0f);

            T obj = GetInstance();

            Vector3 direction = new Vector3(Mathf.Cos(spawnAngle * Mathf.Deg2Rad), Mathf.Sin(spawnAngle * Mathf.Deg2Rad), 0.0f);
            Vector3 newPos = transform.position + (spawnDistance * direction);

            obj.transform.position = newPos;

            OnSpawnObject(obj);

            Debug.LogFormat("Spawned : {0} Spawn Distance : {1}, Spawn Angle : {2}", obj.name, spawnDistance, spawnAngle);

            if (!EnableLooping)
            {
                IsSpawning = false;
            }
        }

        /// <summary>
        /// Randomly spawns A object each time when the method is called
        /// base on the <see cref="SpawnInnerSize"/> and <see cref="SpawnOuterSize"/> of a square.
        /// </summary>
        protected virtual void SqaureSpawn()
        {
            if (!AllowSpawnMore && _spawningCount >= MaxObjectCount)
            {
                return;
            }

            float posX = Random.Range(-SpawnOuterSize, SpawnOuterSize);
            float posY = Random.Range(-SpawnOuterSize, SpawnOuterSize);

            float clampX = Mathf.Clamp(posX.Abs(), SpawnInnerSize, SpawnOuterSize);
            float clampY = Mathf.Clamp(posY.Abs(), SpawnInnerSize, SpawnOuterSize);

            posX = clampX * posX.Sign();
            posY = clampY * posY.Sign();

            T obj = GetInstance();

            Vector3 newPos = transform.position + new Vector3(posX, posY, 0.0f);

            obj.transform.position = newPos;

            OnSpawnObject(obj);

            Debug.LogFormat("Spawned : {0} PosX : {1}, PosY : {2}", obj.name, posX, posY);

            if (!EnableLooping)
            {
                IsSpawning = false;
            }
        }

        /// <summary>
        /// Randomly spawns A object each time when the method is called
        /// base on the <see cref="SpawnPoints"/>
        /// </summary>
        protected virtual void PointSpawn()
        {
            if (SpawnPoints.Count == 0)
            {
                return;
            }
            else if (!AllowSpawnMore && _spawningCount >= MaxObjectCount)
            {
                return;
            }

            int index = Random.Range(0, SpawnPoints.Count);

            T obj = GetInstance();

            CurrentSpawnedPoint = SpawnPoints[index];
            obj.transform.position = CurrentSpawnedPoint.position;

            OnSpawnObject(obj);

            Debug.LogFormat("Spawned : {0} Point Index : {1}, Pos : {2}", obj.name, index, SpawnPoints[index].position);

            if (!EnableLooping)
            {
                IsSpawning = false;
            }
        }
        #endregion

        /// <summary>
        /// Manipulates the object when it has being spawned.
        /// </summary>
        /// <param name="obj">The object has being spawned</param>
        public abstract void OnSpawnObject(T obj);
    }
}