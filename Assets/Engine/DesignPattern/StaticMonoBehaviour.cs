﻿using UnityEngine;
using System.Collections;
using System;

namespace KYIEngine.DesignPattern
{
    /// <summary>
    /// Makes a instance as singletion but destory on scene changed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="UnityEngine.MonoBehaviour" />
    public abstract class StaticMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance { get; private set; }
        
        /// <summary>
        /// Unity message <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.Awake.html">Awake</a>.<para/>
        /// The basic singleton management process occurs.
        /// </summary>
        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<T>();
            }
            else if (Instance != this)
            {
                // Only first instanced object will be alive.
                Destroy(gameObject);
                return;
            }
        }
        protected virtual void OnEnable()
        {
            var instancelist = GameObject.FindObjectsOfType<T>();
            if (instancelist.Length > 1)
            {
                Debug.LogErrorFormat("There are more than one instances : {0}. This is not supported.", typeof(T).Name);
            }
        }
    }
}