﻿using System;
using UnityEngine;

namespace KYIEngine.DesignPattern
{
    /// <summary>
    /// Makes instance as a singleton.
    /// </summary>
    /// <typeparam name="T">The type of FrameworkMono</typeparam>
    /// <seealso cref="MobileFramework.FrameworkMono" />
    /// <seealso href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.html">UnityEngine.MonoBehaviour</seealso>
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        public static T Instance { get; private set; }
        
        /// <summary>
        /// Unity message <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.Awake.html">Awake</a>.<para/>
        /// The basic singleton management process occurs.
        /// </summary>
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = gameObject.GetComponent<T>();
            }
            else if (Instance != this)
            {
                // Only first instanced object will be alive.
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }
        protected virtual void OnEnable()
        {
            var instancelist = GameObject.FindObjectsOfType<T>();
            if (instancelist.Length > 1)
            {
                Debug.LogErrorFormat("There are more than one instances : {0}. This is not supported.", typeof(T).Name);
            }
        }
    }
}
