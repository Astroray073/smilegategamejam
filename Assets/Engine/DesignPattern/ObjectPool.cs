﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace KYIEngine.DesignPattern
{
    /// <summary>
    /// General object pool interface.
    /// </summary>
    /// <typeparam name="T">The type of object to pooling.</typeparam>
    /// <seealso cref="MobileFramework.FrameworkMono" />
    /// <seealso href="https://unity3d.com/kr/learn/tutorials/modules/beginner/live-training-archive/object-pooling">Object Polling Tutorial</seealso>
    public abstract class ObjectPool<T> : MonoBehaviour where T : Component
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is initialized.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
        /// </value>
        public bool IsInitialized { get; private set; }

        /// <summary>
        /// Gets the spawning count.
        /// </summary>
        /// <value>
        /// The spawning count.
        /// </value>
        protected int _spawningCount
        {
            get
            {
                int count = 0;
                for (int i = 0; i < MaxObjectCount; i++)
                {
                    if (_objectPool[i].activeInHierarchy)
                    {
                        count++;
                    }
                }
                return count;
            }
        }
        #endregion

        #region Public Fields
        /// <summary>
        /// Initialize on Awake.
        /// </summary>
        public bool ForcedInitialize = false;

        /// <summary>
        /// The maximum object count.
        /// </summary>
        public int MaxObjectCount;

        /// <summary>
        /// The allocate size when it needs to resize the pool.
        /// </summary>
        public int AllocationSize = 5;

        /// <summary>
        /// The object prefab to pool.
        /// </summary>
        public T ObjectPrefab;
        #endregion

        #region Private Fields
        /// <summary>
        /// The object pool.
        /// </summary>
        private GameObject[] _objectPool;

        /// <summary>
        /// The object group.
        /// </summary>
        private Transform _objectGroup;

        /// <summary>
        /// The instantiate object helper.
        /// </summary>
        private Func<T, int, GameObject> _instantiateObject;
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the pool.
        /// </summary>
        public void InitializePool()
        {
            if (IsInitialized)
            {
                Debug.LogWarning("You already initialized the pool. This attempt is canceled.");
                return;
            }

            _objectPool = new GameObject[MaxObjectCount];
            for (int i = 0; i < MaxObjectCount; i++)
            {
                _objectPool[i] = _instantiateObject(ObjectPrefab, i);
            }
            IsInitialized = true;
            Debug.Log("Pool is Initialized.");
        }

        /// <summary>
        /// Get the game object from the pool.
        /// </summary>
        /// <returns>The game object to activate.</returns>
        public GameObject GetGameObject()
        {
            GameObject instance = null;

            if (_spawningCount < MaxObjectCount)
            {
                for (int i = 0; i < MaxObjectCount; i++)
                {
                    if (!_objectPool[i].activeInHierarchy)
                    {
                        _objectPool[i].SetActive(true);
                        instance = _objectPool[i];
                        break;
                    }
                }

                return instance;
            }
            else
            {
                // Need More... resize
                int searchIndex = MaxObjectCount;
                IncreaseSize();

                for (int i = searchIndex; i < MaxObjectCount; i++)
                {
                    if (!_objectPool[i].activeInHierarchy)
                    {
                        _objectPool[i].SetActive(true);
                        instance = _objectPool[i];
                        break;
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Get the specified instance from the pool.
        /// </summary>
        /// <returns>The specified instance to activate.</returns>
        public T GetInstance()
        {
            GameObject instance = null;

            if (_spawningCount < MaxObjectCount)
            {
                for (int i = 0; i < MaxObjectCount; i++)
                {
                    if (!_objectPool[i].activeInHierarchy)
                    {
                        _objectPool[i].SetActive(true);
                        instance = _objectPool[i];
                        break;
                    }
                }

                return instance.GetComponent<T>();
            }
            else
            {
                Debug.Log("Cannot find available object. Increase the size of the pool.");
                int searchIndex = MaxObjectCount;
                IncreaseSize();

                for (int i = searchIndex; i < MaxObjectCount; i++)
                {
                    if (!_objectPool[i].activeInHierarchy)
                    {
                        _objectPool[i].SetActive(true);
                        instance = _objectPool[i];
                        break;
                    }
                }

                return instance.GetComponent<T>();
            }
        }

        /// <summary>
        /// Gets the spawned objects.
        /// </summary>
        /// <returns>The list of the spawned objects.</returns>
        public List<T> GetSpawnedObjects()
        {
            List<T> spawnedObjects = new List<T>();

            for (int i = 0; i < MaxObjectCount; i++)
            {
                if (_objectPool[i].activeInHierarchy)
                {
                    spawnedObjects.Add(_objectPool[i].GetComponent<T>());
                }
            }

            return spawnedObjects;
        }

        /// <summary>
        /// Set the spawned objects to be inactive.
        /// </summary>
        public void Clear()
        {
            for (int i = 0; i < MaxObjectCount; i++)
            {
                if (_objectPool[i].activeInHierarchy)
                {
                    _objectPool[i].SetActive(false);
                }
            }
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Unity message <a href="http://docs.unity3d.com/540/Documentation/ScriptReference/MonoBehaviour.Awake.html">Awake</a>.
        /// </summary>
        protected virtual void Awake()
        {
            IsInitialized = false;

            // assign helper method.
            _instantiateObject = (prefab, i) =>
            {
                if (!_objectGroup)
                {
                    _objectGroup = new GameObject(name + "Group").transform;
                }

                var go = GameObject.Instantiate<T>(prefab).gameObject;
                go.name = prefab.name + " " + i;
                go.transform.SetParent(_objectGroup);
                go.SetActive(false);

                return go;
            };

            if (ForcedInitialize)
            {
                InitializePool();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Increases the size of the pool by the <c cref="AllocationSize"/>.
        /// </summary>
        private void IncreaseSize()
        {
            MaxObjectCount += AllocationSize;

            var tempObjects = _objectPool;

            _objectPool = new GameObject[MaxObjectCount];

            for (int i = 0; i < MaxObjectCount; i++)
            {
                if (i < tempObjects.Length)
                {
                    _objectPool[i] = tempObjects[i];
                }
                else
                {
                    _objectPool[i] = _instantiateObject(ObjectPrefab, i);
                }
            }
        }
        #endregion
    }
}