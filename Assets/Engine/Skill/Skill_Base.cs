﻿using UnityEngine;
using System.Collections;

public class Skill_Base : MonoBehaviour {

    [SerializeField]
    protected float coolTime;     // 쿨타임 맥스

    [SerializeField]
    protected SkillButtonUI buttonUI;

    public static bool prevent = false;

    public virtual void OnButtonClick() { }
}
