﻿using UnityEngine;
using System.Collections;

public class Skill_DefenderSpecial : Skill_Base
{

    public GameObject defenderEffect;

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnButtonClick()
    {
        base.OnButtonClick();

        // 바로 발동
        buttonUI.SetCoolTime(coolTime);

        CP.Plane[] secondFloor = GameManager.Gamemanager.Map.PlaneOnFloor[GameManager.Gamemanager.Map.PlaneOnFloor.Count - 2];
        CheckPlayer(secondFloor);
        CP.Plane[] thirdFloor = GameManager.Gamemanager.Map.PlaneOnFloor[GameManager.Gamemanager.Map.PlaneOnFloor.Count - 3];
        CheckPlayer(thirdFloor);

        GameObject clone = GameObject.Instantiate(defenderEffect as UnityEngine.Object) as GameObject;
        if (clone != null)
            clone.SetActive(true);

        StartCoroutine(RemoveEffect(clone));
    }

    public IEnumerator RemoveEffect(GameObject effect)
    {
        yield return new WaitForSeconds(coolTime);

        GameObject.DestroyImmediate(effect);
    }

    public void CheckPlayer(CP.Plane[] floor)
    {
        if (floor == null)
            return;

        CP.Plane p1 = GameManager.Gamemanager.Map.GetPlane(GameManager.Gamemanager.PlayerA1.transform.position);
        CP.Plane p2 = GameManager.Gamemanager.Map.GetPlane(GameManager.Gamemanager.PlayerA2.transform.position);

        foreach (var plane in floor)
        {
            if (p1 != null && p1 == plane)
            {
                GameManager.Gamemanager.PlayerA1.AttackedMoveDown(2);
            }
            if (p2 != null && p2 == plane)
            {
                GameManager.Gamemanager.PlayerA2.AttackedMoveDown(2);
            }
        }

    }
}
