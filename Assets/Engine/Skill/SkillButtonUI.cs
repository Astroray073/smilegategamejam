﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkillButtonUI : MonoBehaviour {

    [SerializeField]
    private Skill_Base skill;

    private Text coolTimeText;

    private float coolTime;

    public KeyCode keyInput = KeyCode.None;

	// Use this for initialization
	void Start () {

        GetComponent<Button>().onClick.AddListener(OnButtonClick);

        coolTimeText = transform.FindChild("Text").GetComponent<Text>();
        coolTimeText.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        coolTimeText.text = coolTime.ToString("00");

        if (coolTime > 0)
        {
            coolTime -= Time.deltaTime;

            if (coolTime <= 0)
            {
                coolTime = 0f;
                coolTimeText.gameObject.SetActive(false);
            }
        }

        if (Input.GetKeyDown(keyInput))
        {
            OnButtonClick();
        }
    }

    void OnButtonClick()
    {
        Debug.Log("click");

        if (coolTime > 0)
            return;

        if (Skill_Base.prevent)
            return;

        skill.OnButtonClick();
    }

    public void SetCoolTime(float time)
    {
        coolTime = time;

        coolTimeText.gameObject.SetActive(true);
    }
}
