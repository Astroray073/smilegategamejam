﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Skill_Confusion : Skill_Base
{
    public Button playerSlotUI_1;
    public Button playerSlotUI_2;

    private bool onActive;

    public float reverseTime = 3f;

    // Use this for initialization
    void Start()
    {
        // 
        playerSlotUI_1.interactable = false;
        playerSlotUI_1.onClick.AddListener(ClickPlayer1);
        playerSlotUI_2.interactable = false;
        playerSlotUI_2.onClick.AddListener(ClickPlayer2);
    }

    public void ClickPlayer1()
    {
        GameManager.Gamemanager.PlayerA1.MoveRevers(reverseTime);

        playerSlotUI_1.interactable = false;
        playerSlotUI_2.interactable = false;

        Skill_Base.prevent = false;
        onActive = false;
        buttonUI.SetCoolTime(coolTime);
    }

    public void ClickPlayer2()
    {
        GameManager.Gamemanager.PlayerA2.MoveRevers(reverseTime);

        playerSlotUI_1.interactable = false;
        playerSlotUI_2.interactable = false;

        Skill_Base.prevent = false;
        onActive = false;
        buttonUI.SetCoolTime(coolTime);
    }

    public override void OnButtonClick()
    {
        base.OnButtonClick();

        onActive = true;
        Skill_Base.prevent = true;

        playerSlotUI_1.interactable = true;
        playerSlotUI_2.interactable = true;
    }
}
