﻿using UnityEngine;
using System.Collections;

public class Skill_Pad : Skill_Base
{
    private bool onAction = false;

    [SerializeField]
    private int blockTime = 3;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
        if (onAction)
        {
            // 장판깔기 이팩트 disable;
            foreach (var line in GameManager.Gamemanager.Map.PlaneOnFloor)
            {
                foreach(var slot in line)
                {
                    SetOverEffect(slot, false);
                }
            }

            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var plane = GameManager.Gamemanager.Map.GetPlane(mousePos);
            if (plane == null) return;

            SetOverEffect(plane, true);
            SetOverEffect(plane.BottomPlane, true);
            SetOverEffect(plane.UpPlane, true);
            SetOverEffect(plane.LeftPlane, true);
            SetOverEffect(plane.RightPlane, true);

            if (Input.GetMouseButtonUp(0))
            {
                CP.Plane p1 = GameManager.Gamemanager.Map.GetPlane(GameManager.Gamemanager.PlayerA1.transform.position);
                CP.Plane p2 = GameManager.Gamemanager.Map.GetPlane(GameManager.Gamemanager.PlayerA2.transform.position);

                if (p1 == plane || p1 == plane.UpPlane || p1 == plane.UpPlane || p1 == plane.LeftPlane || p1 == plane.RightPlane)
                {
                    // 그 안에 위치한 플레이어 발동..!
                    GameManager.Gamemanager.PlayerA1.DontMovd(blockTime);
                }
                if (p2 == plane || p2 == plane.UpPlane || p2 == plane.UpPlane || p2 == plane.LeftPlane || p2 == plane.RightPlane)
                {
                    // 그 안에 위치한 플레이어 발동..!
                    GameManager.Gamemanager.PlayerA2.DontMovd(blockTime);
                }

                buttonUI.SetCoolTime(coolTime);
                onAction = false;
                Skill_Base.prevent = false;

                Debug.Log("click");

                // 장판깔기 이팩트 disable;
                foreach (var line in GameManager.Gamemanager.Map.PlaneOnFloor)
                {
                    foreach (var slot in line)
                    {
                        SetOverEffect(slot, false);
                    }
                }
            }
        }
	}

    public override void OnButtonClick()
    {
        base.OnButtonClick();

        onAction = true;
        Skill_Base.prevent = true;
    }
    private void SetOverEffect(CP.Plane plane, bool active)
    {
        if (plane == null)
            return;

        int[] id = GameManager.Gamemanager.Map.GetPlaneID(plane);

        GameObject obj = GameManager.Gamemanager.Map.PlaneTileOnFloor[id[0]][id[1]];
        if (obj == null)
            return;


        obj.transform.FindChild("pad").gameObject.SetActive(active);
    }
}
