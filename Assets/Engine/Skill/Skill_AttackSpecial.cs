﻿using UnityEngine;
using System.Collections;

public enum AttackerPlayerType
{
    PlayerA1,
    PlayerA2,
}

public class Skill_AttackSpecial : Skill_Base
{
    public AttackerPlayerType playerType = 0;
    

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnButtonClick()
    {
        base.OnButtonClick();

        // 바로 발동
        buttonUI.SetCoolTime(coolTime);
        
        if (playerType == AttackerPlayerType.PlayerA1)
        {
            CheckPlayer(GameManager.Gamemanager.PlayerA1);
        }
        else if (playerType == AttackerPlayerType.PlayerA2)
        {
            CheckPlayer(GameManager.Gamemanager.PlayerA2);
        }
    }

    private void CheckPlayer(PlayerMove player)
    {
        int randomY = Random.Range(0, player.MaxFloor + 1);

        CP.Plane[] planes = GameManager.Gamemanager.Map.PlaneOnFloor[randomY];

        int randomX = Random.Range(0, planes.Length);
        CP.Plane randomPlane;
        while (true)
        {
            randomX = Random.Range(0, planes.Length);
            randomPlane = GameManager.Gamemanager.Map.PlaneOnFloor[randomY][randomX];

            if (randomPlane != player.currentPlane
                && randomPlane != player.PresentPlane)
                break;
        }

        Vector2 planeVector = randomPlane.Center;
        player.transform.position = new Vector3(planeVector.x, planeVector.y, player.transform.position.z);

        player.MoveFlag = false;
        player.MoveDelay = 0f;
        player.currentPlane = randomPlane;
        player.PresentPlane = randomPlane;

        player.transform.FindChild("offense").gameObject.SetActive(false);
        player.transform.FindChild("offense").gameObject.SetActive(true);
    }

}
