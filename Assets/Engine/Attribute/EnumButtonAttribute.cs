﻿using UnityEngine;
using System.Collections;
using System;

namespace KYIEngine
{
    public class EnumButtonAttribute : IndentAttribute
    {
        public EnumButtonAttribute(bool indent = false) : base(indent) { }
    }
}