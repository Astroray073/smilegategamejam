﻿// ***********************************************************************
// Assembly         : Assembly-CSharp
// Author           : IN-YEOL, CHOI
// Created          : 10-13-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="SerializationFormatterAttribute.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace KYIEngine.Serialization
{
    /// <summary>
    /// Marks this formatter class of the <see cref="SerializationFormatter{T}.TargetType"/> is not overridable.
    /// If this attribute attached certain type of formatter, the system will be ignore other formatters of the type and use this formatter.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    /// <seealso cref="SerializationFormatter{T}.TargetType"/>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class PreventCustomFormatter : Attribute { }

    /// <summary>
    /// Indicate this formatter class should be used instead of the default formatter.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class OverrideFormatterAttribute : Attribute { }

    #region Regacy
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CustomFormatter : Attribute
    {
        public Type Target { get { return _target; } }
        private Type _target;

        public CustomFormatter(Type type)
        {
            _target = type;
        }
    }
    
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class FormatterOfAttribute : Attribute
    {
        public Type Target { get { return _target; } }
        private Type _target;

        public FormatterOfAttribute(Type type)
        {
            _target = type;
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class UseFormatter : Attribute
    {
        public Type FormatterType { get { return _formatterType; } }
        //public bool Inherit { get { return _inherit; } }
        private Type _formatterType;
        //private bool _inherit;

        public UseFormatter(Type formatterType)//, bool inherit = false)
        {
            _formatterType = formatterType;
          //  _inherit = inherit;
        }
    }
    #endregion
}