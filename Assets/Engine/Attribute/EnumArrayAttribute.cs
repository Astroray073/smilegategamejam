﻿using UnityEngine;
using System;

namespace KYIEngine
{
    /// <summary>
    /// Make fixed arrays to fit enum.
    /// </summary>
    /// <seealso cref="UnityEngine.PropertyAttribute" />
    public class EnumArrayAttribute : PropertyAttribute
    {
        /// <summary>
        /// The enum names
        /// </summary>
        public string[] EnumNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="EnumArrayAttribute"/> class.
        /// </summary>
        /// <param name="enumType">Type of the enum.</param>
        public EnumArrayAttribute(Type enumType)
        {
            EnumNames = Enum.GetNames(enumType);
        }
    }
}