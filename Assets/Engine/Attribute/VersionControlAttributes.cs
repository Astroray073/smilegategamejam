﻿// ***********************************************************************
// Assembly         : Assembly-CSharp
// Author           : IN-YEOL, CHOI
// Created          : 10-16-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="ModifiedAttribute.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace KYIEngine
{
    /// <summary>
    /// Storing change info through version.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
    public class ModifiedAttribute : Attribute
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        public string Version { get { return _version; } }
        /// <summary>
        /// Gets the change log.
        /// </summary>
        /// <value>The change log.</value>
        public string ChangeLog { get { return _changeLog; } }

        /// <summary>
        /// The version
        /// </summary>
        private string _version;
        /// <summary>
        /// The change log
        /// </summary>
        private string _changeLog;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedAttribute"/> class.
        /// </summary>
        /// <param name="version">The version.</param>
        /// <param name="changeLog">The message to store.</param>
        public ModifiedAttribute(string version, string changeLog)
        {
            _version = version;
            _changeLog = changeLog;
        }
    }

    /// <summary>
    /// Marks code added in certain version.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
    public class Added : Attribute
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>The version.</value>
        public string Version { get { return _version; } }

        /// <summary>
        /// The version
        /// </summary>
        private string _version;

        /// <summary>
        /// Initializes a new instance of the <see cref="Added"/> class.
        /// </summary>
        /// <param name="version">The version.</param>
        public Added(string version)
        {
            _version = version;
        }
    }
}