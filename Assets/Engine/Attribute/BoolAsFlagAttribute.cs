﻿using System;
using UnityEngine;

namespace KYIEngine
{
    /// <summary>
    /// Boolean as flag
    /// </summary>
    /// <seealso cref="UnityEngine.PropertyAttribute" />
    public class BoolAsFlagAttribute : PropertyAttribute
    {
        /// <summary>
        /// The enum names
        /// </summary>
        public string[] EnumNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="BoolAsFlagAttribute"/> class.
        /// </summary>
        /// <param name="enumType">Type of the enum.</param>
        public BoolAsFlagAttribute(Type enumType)
        {
            EnumNames = Enum.GetNames(enumType);
        }
    }
}