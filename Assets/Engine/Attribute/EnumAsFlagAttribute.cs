﻿using UnityEngine;

namespace KYIEngine
{
    /// <summary>
    /// The attribute makes button shape flag selector UI.
    /// </summary>
    /// <seealso cref="UnityEngine.PropertyAttribute" />
    public class EnumAsFlagAttribute : PropertyAttribute { }
}