﻿using UnityEngine;
using System.Collections;

namespace KYIEngine
{
    public class IndentAttribute : PropertyAttribute
    {
        public bool Indent { get; private set; }
        public IndentAttribute(bool indent = true)
        {
            Indent = indent;
        }
    }
    public class CustomLabelAttribute : IndentAttribute
    {
        public string Label { get; private set; }
        public CustomLabelAttribute(string label, bool indent = false) : base(indent)
        {
            Label = label;
        }
    }
    public class IncrementAttribute : CustomLabelAttribute
    {
        public float Increment { get; private set; }
        public IncrementAttribute(float increment = 0.5f, bool indent = false, string label = "") : base(label, indent)
        {
            Increment = increment;
        }
    }
    public class NonNegativeValueAttribute : IncrementAttribute
    {
        public NonNegativeValueAttribute(float increment = 0.5f, bool indent = false, string label = "") : base(increment, indent, label) { }
    }
    public class NotEditableAttribute : CustomLabelAttribute
    {
        public NotEditableAttribute(bool indent = false, string label = "") : base(label, indent) { }
    }
    public class NoLabelAttribute : IndentAttribute
    {
        public NoLabelAttribute(bool indent = false) : base(indent) { }
    }
}