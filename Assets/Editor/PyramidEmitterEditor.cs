﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace CPEdtior
{
    [CustomEditor(typeof(CP.PyramidEmitter))]
    public class PyramidEmitterEditor : Editor
    {
        private ReorderableList _emitterTiming;
        private void OnEnable()
        {
            _emitterTiming = KYIEditor.ReorderableListUtility.CreateSimpleReorderableList(serializedObject, serializedObject.FindProperty("_emitterTiming"));
        }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_minEmitterForce"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_maxEmitterForce"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_limitFloorCount"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_jitteringTime"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_jitteringAmount"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_jitteringRepeatTime"));
            EditorGUILayout.Space();
            _emitterTiming.DoLayoutList();

            serializedObject.ApplyModifiedProperties();    
        }
    }
}