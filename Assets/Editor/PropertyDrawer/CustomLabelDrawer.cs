﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(CustomLabelAttribute))]
    public class CustomLabelDrawer : IndentDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawCustomLabelField(position, property, label);
        }

        protected void DrawCustomLabelField(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = attribute as CustomLabelAttribute;
            if (attr.Indent)
            {
                if (string.IsNullOrEmpty(attr.Label))
                {
                    DrawIndentedField(position, property, label);
                }
                else
                {
                    DrawIndentedField(position, property, new GUIContent(attr.Label));
                }
            }
            else
            {
                if (string.IsNullOrEmpty(attr.Label))
                {
                    EditorGUI.PropertyField(position, property, label);
                }
                else
                {
                    EditorGUI.PropertyField(position, property, new GUIContent(attr.Label));
                }
            }
        }
    }
}