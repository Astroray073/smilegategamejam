﻿// ***********************************************************************
// Assembly         : Assembly-CSharp-Editor
// Author           : IN-YEOL, CHOI
// Created          : 10-23-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-23-2016
// ***********************************************************************
// <copyright file="NonNegativeValueDrawer.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// Property drawer for <see cref="NonNegativeValueAttribute"/>.
    /// </summary>
    /// <seealso cref="UnityEditor.PropertyDrawer" />
    [CustomPropertyDrawer(typeof(NonNegativeValueAttribute))]
    public class NonNegativeValueAttributeDrawer : IncrementDrawer
    {
        /// <summary>
        /// Called when <paramref name="property" /> value changed.
        /// </summary>
        /// <param name="property">The property.</param>
        protected override void OnValueChanged(SerializedProperty property)
        {
            if (property.propertyType == SerializedPropertyType.Integer
                && property.intValue < 0)
                property.intValue = 0;

            if (property.propertyType == SerializedPropertyType.Float
                && property.floatValue < 0.0f)
                property.floatValue = 0.0f;
        }
    }
}