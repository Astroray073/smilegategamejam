﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(EnumArrayAttribute))]
    public class EnumArrayAttributeDrawer : PropertyDrawer
    {
        /// <summary>
        /// The show array for fold out.
        /// </summary>
        private bool _showArray = true;
        
        /// <summary>
        /// Override this method to make your own GUI for the property.
        /// </summary>
        /// <param name="position">Rectangle on the screen to use for the property GUI.</param>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EnumArrayAttribute attr = attribute as EnumArrayAttribute;

            int enumLength = attr.EnumNames.Length;
            float singleWidth = position.width / enumLength;
            float singleHeight = _showArray ? position.height / 3 : position.height;

            _showArray = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, singleHeight), _showArray, label);

            if (_showArray)
            {
                EditorGUI.BeginChangeCheck();

                property = property.FindPropertyRelative("Array");
                property.arraySize = enumLength;

                for (int i = 0; i < enumLength; i++)
                {
                    Rect enumNamePos = new Rect(position.x + singleWidth * i, position.y + singleHeight, singleWidth, singleHeight);
                    EditorGUI.LabelField(enumNamePos, attr.EnumNames[i]);

                    Rect contentPos = new Rect(position.x + singleWidth * i, position.y + singleHeight * 2, singleWidth, singleHeight);
                    EditorGUI.PropertyField(contentPos, property.GetArrayElementAtIndex(i), GUIContent.none);
                }

                EditorGUI.EndChangeCheck();
            }
        }

        /// <summary>
        /// Override this method to specify how tall the GUI for this field is in pixels.
        /// </summary>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        /// <returns>
        /// The height in pixels.
        /// </returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return _showArray ? base.GetPropertyHeight(property, label) * 3 : base.GetPropertyHeight(property, label);
        }
    }
}
