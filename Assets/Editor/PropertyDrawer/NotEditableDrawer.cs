﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(NotEditableAttribute))]
    public class NotEditableDrawer : CustomLabelDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GUI.enabled = false;
            DrawCustomLabelField(position, property, label);
            GUI.enabled = true;
        }
    }
}