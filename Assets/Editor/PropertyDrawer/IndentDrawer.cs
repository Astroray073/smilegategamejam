﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(IndentAttribute))]
    public class IndentDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            DrawIndentedField(position, property, label);
        }
        protected void DrawIndentedField(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.indentLevel++;
            EditorGUI.PropertyField(position, property, label);
            EditorGUI.indentLevel--;
        }
    }
}