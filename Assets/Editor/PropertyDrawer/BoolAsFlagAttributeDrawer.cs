﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// Custom property drawer for <see cref="BoolAsFlagAttribute" />
    /// </summary>
    /// <seealso cref="UnityEditor.PropertyDrawer" />
    [CustomPropertyDrawer(typeof(BoolAsFlagAttribute))]
    public class BoolAsFlagAttributeDrawer : PropertyDrawer
    {
        /// <summary>
        /// Override this method to make your own GUI for the property.
        /// </summary>
        /// <param name="position">Rectangle on the screen to use for the property GUI.</param>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = attribute as BoolAsFlagAttribute;
            
            int enumLength = attr.EnumNames.Length;
            float buttonWidth = position.width / enumLength;
            float singleHeight = position.height / 2;
            
            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, singleHeight), label);

            EditorGUI.BeginChangeCheck();

            property = property.FindPropertyRelative("Array");
            property.arraySize = enumLength;
            
            for (int i = 0; i < enumLength; i++)
            {
                Rect buttonPos = new Rect(position.x + buttonWidth * i, position.y + singleHeight, buttonWidth, singleHeight);
                property.GetArrayElementAtIndex(i).boolValue = GUI.Toggle(buttonPos, property.GetArrayElementAtIndex(i).boolValue, attr.EnumNames[i], "Button");
            }
            
            EditorGUI.EndChangeCheck();
        }

        /// <summary>
        /// Override this method to specify how tall the GUI for this field is in pixels.
        /// </summary>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        /// <returns>
        /// The height in pixels.
        /// </returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 2;
        }
    }
}