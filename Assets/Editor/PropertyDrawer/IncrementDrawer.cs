﻿// ***********************************************************************
// Assembly         : Assembly-CSharp-Editor
// Author           : IN-YEOL, CHOI
// Created          : 10-23-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-23-2016
// ***********************************************************************
// <copyright file="IncrementDrawer.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// Property drawer for <see cref="IncrementAttribute"/>
    /// </summary>
    /// <seealso cref="UnityEditor.PropertyDrawer" />
    [CustomPropertyDrawer(typeof(IncrementAttribute))]
    public class IncrementDrawer : PropertyDrawer
    {
        /// <summary>
        /// Override this method to make your own GUI for the property.
        /// </summary>
        /// <param name="position">Rectangle on the screen to use for the property GUI.</param>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = attribute as IncrementAttribute;

            GUIContent propertyLabel;
            if (string.IsNullOrEmpty(attr.Label))
            {
                propertyLabel = label;
            }
            else
            {
                propertyLabel = new GUIContent(attr.Label);
            }

            if (attr.Indent)
            {
                EditorGUI.indentLevel++;
                position = EditorGUI.IndentedRect(position);
                EditorGUI.indentLevel--;
            }

            using (var scope = new KYIEditorGUILayout.HorizontalScope(position))
            {
                EditorGUI.PropertyField(scope.GetRectRatio(0.8f), property, propertyLabel);
                if (GUI.Button(scope.GetRectRatio(0.1f), "-", EditorStyles.miniButtonLeft))
                {
                    if (property.propertyType == SerializedPropertyType.Integer)
                    {
                        property.intValue -= (int)attr.Increment;
                        OnValueChanged(property);
                    }
                    if (property.propertyType == SerializedPropertyType.Float)
                    {
                        property.floatValue -= attr.Increment;
                        OnValueChanged(property);
                    }
                }

                if (GUI.Button(scope.GetRemainingRect(), "+", EditorStyles.miniButtonRight))
                {
                    if (property.propertyType == SerializedPropertyType.Integer)
                    {
                        property.intValue += (int)attr.Increment;
                        OnValueChanged(property);
                    }
                    if (property.propertyType == SerializedPropertyType.Float)
                    {
                        property.floatValue += attr.Increment;
                        OnValueChanged(property);
                    }
                }
            }

        }

        /// <summary>
        /// Called when <paramref name="property"/> value changed.
        /// </summary>
        /// <param name="property">The property.</param>
        protected virtual void OnValueChanged(SerializedProperty property) { }
    }
}