﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(NoLabelAttribute))]
    public class NoLabelDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            base.OnGUI(position, property, GUIContent.none);
        }
    }
}