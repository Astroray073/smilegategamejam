﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// Custom property drawer for <see cref="EnumAsFlagAttribute"/>
    /// </summary>
    /// <seealso cref="UnityEditor.PropertyDrawer" />
    [CustomPropertyDrawer(typeof(EnumAsFlagAttribute))]
    public class EnumAsFlagAttributeDrawer : PropertyDrawer
    {
        /// <summary>
        /// Override this method to make your own GUI for the property.
        /// </summary>
        /// <param name="position">Rectangle on the screen to use for the property GUI.</param>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            int buttonsIntValue = 0;
            int enumLength = property.enumNames.Length;
            bool[] buttonPressed = new bool[enumLength];
            float buttonWidth = position.width / enumLength;
            float singleHeight = position.height / 2;

            EditorGUI.LabelField(new Rect(position.x, position.y, position.width, singleHeight), label);
            
            EditorGUI.BeginChangeCheck();

            for (int i = 0; i < enumLength; i++)
            {

                // Check if the button is/was pressed 
                if ((property.intValue & (1 << i)) == 1 << i)
                {
                    buttonPressed[i] = true;
                }

                Rect buttonPos = new Rect(position.x + buttonWidth * i, position.y + singleHeight, buttonWidth, singleHeight);

                buttonPressed[i] = GUI.Toggle(buttonPos, buttonPressed[i], property.enumNames[i], "Button");

                if (buttonPressed[i])
                    buttonsIntValue += 1 << i;
            }

            if (EditorGUI.EndChangeCheck())
            {
                property.intValue = buttonsIntValue;
            }
        }

        /// <summary>
        /// Override this method to specify how tall the GUI for this field is in pixels.
        /// </summary>
        /// <param name="property">The SerializedProperty to make the custom GUI for.</param>
        /// <param name="label">The label of this property.</param>
        /// <returns>
        /// The height in pixels.
        /// </returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 2;
        }
    }
}