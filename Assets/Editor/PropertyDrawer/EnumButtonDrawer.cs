﻿using UnityEngine;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    [CustomPropertyDrawer(typeof(EnumButtonAttribute))]
    public class EnumButtonDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attr = attribute as EnumButtonAttribute;
            
            if (attr.Indent)
            {
                EditorGUI.indentLevel++;
                position = EditorGUI.IndentedRect(position);
                EditorGUI.indentLevel--;
            }
            
            if (property.propertyType == SerializedPropertyType.Enum)
            {
                var enumNames = property.enumDisplayNames;
                using (var scope = new KYIEditorGUILayout.HorizontalScope(position))
                {
                    EditorGUI.LabelField(scope.GetRect(EditorGUIUtility.labelWidth), label);
                    for (int i = 0; i < enumNames.Length; i++)
                    {
                        GUIStyle style;

                        if (i == 0)
                        {
                            style = EditorStyles.miniButtonLeft;
                        }
                        else if (i == enumNames.Length - 1)
                        {
                            style = EditorStyles.miniButtonRight;
                        }
                        else
                        {
                            style = EditorStyles.miniButtonMid;
                        }

                        if (GUI.Toggle(scope.GetRectEven(enumNames.Length - i), property.enumValueIndex == i, enumNames[i], style))
                        {
                            property.enumValueIndex = i;
                        }
                    }
                }
            }
            else
            {
                EditorGUI.LabelField(position, label.text, "Use with enum.");
            }
        }
    }
}