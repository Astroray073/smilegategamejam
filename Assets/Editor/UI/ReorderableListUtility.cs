﻿// ***********************************************************************
// Assembly         : Assembly-CSharp-Editor
// Author           : IN-YEOL, CHOI
// Created          : 10-13-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="ReorderableListUtility.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace KYIEditor
{
    /// <summary>
    /// Utility class for <see cref="ReorderableList"/>
    /// </summary>
    public static class ReorderableListUtility
    {
        /// <summary>
        /// Creates the simple reorderable list.
        /// Use this for create 1D list or array.
        /// </summary>
        /// <param name="serializedObject">The serialized object.</param>
        /// <param name="elements">The elements.</param>
        /// <returns>ReorderableList.</returns>
        public static ReorderableList CreateSimpleReorderableList(SerializedObject serializedObject, SerializedProperty elements)
        {
            var reorderableList = new ReorderableList(serializedObject, elements);
            reorderableList.drawHeaderCallback += (rect) => DrawHeaderCallback(rect, elements.name);
            reorderableList.drawElementCallback += (rect, index, isActive, isFocused) => DrawElementCallaback(rect, index, isActive, isFocused, elements);

            return reorderableList;
        }
        /// <summary>
        /// Draws the header of <see cref="ReorderableList"/>.
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <param name="label">The label.</param>
        private static void DrawHeaderCallback(Rect rect, string label)
        {
            EditorGUI.LabelField(rect, label);
        }
        /// <summary>
        /// Draws the element of <see cref="ReorderableList"/>
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <param name="index">The index.</param>
        /// <param name="isActive">if set to <c>true</c> [is active].</param>
        /// <param name="isFocused">if set to <c>true</c> [is focused].</param>
        /// <param name="elements">The elements.</param>
        private static void DrawElementCallaback(Rect rect, int index, bool isActive, bool isFocused, SerializedProperty elements)
        {
            EditorGUI.PropertyField(rect, elements.GetArrayElementAtIndex(index), GUIContent.none);
        }
    }
}