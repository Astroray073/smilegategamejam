﻿// ***********************************************************************
// Assembly         : Assembly-CSharp-Editor
// Author           : IN-YEOL, CHOI
// Created          : 10-21-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="KYIEditorStyles.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// Editor styles used in the KYI products.
    /// It copies existing GUIStyles to prevent modifying original GUIStyles.
    /// </summary>
    public static class KYIEditorStyles
    {
        #region Commonly used GUIStyles
        /// <summary>
        /// Gets the preference selection style.
        /// </summary>
        /// <value>The preference selection.</value>
        public static GUIStyle PreferenceSelection { get { return _preferencSelection; } }
        /// <summary>
        /// The preferenc selection style.
        /// </summary>
        private static GUIStyle _preferencSelection;

        /// <summary>
        /// Gets the mid label style.
        /// </summary>
        /// <value>The mid label style.</value>
        public static GUIStyle MidLabel { get { return _midLabel; } }
        /// <summary>
        /// The mid label
        /// </summary>
        private static GUIStyle _midLabel;

        /// <summary>
        /// Gets the reorderable list background style.
        /// </summary>
        /// <value>The reorderable list background style.</value>
        public static GUIStyle ReorderableListBackground { get { return _reorderableListBackground; } }
        /// <summary>
        /// The reorderable list background
        /// </summary>
        private static GUIStyle _reorderableListBackground;

        /// <summary>
        /// Gets the group box style.
        /// </summary>
        /// <value>The group box style.</value>
        public static GUIStyle GroupBox { get { return _groupBox; } }
        /// <summary>
        /// The group box
        /// </summary>
        private static GUIStyle _groupBox;

        #endregion

        #region KYISave GUIStyles
        /// <summary>
        /// Gets the KYISave table header style.
        /// </summary>
        /// <value>The KYISave table header style.</value>
        public static GUIStyle KYISaveTableHeader { get { return _tableHeader; } }
        /// <summary>
        /// The table header
        /// </summary>
        private static GUIStyle _tableHeader;

        /// <summary>
        /// Gets the KYISave sub menu header style.
        /// </summary>
        /// <value>The KYISave sub menu header style.</value>
        public static GUIStyle KYISaveSubMenuHeader { get { return _kyisaveSubMenuHeader; } }
        /// <summary>
        /// The kyisave sub menu header
        /// </summary>
        private static GUIStyle _kyisaveSubMenuHeader;
        #endregion

        #region Textures to make some tweek for guistyles
        /// <summary>
        /// The unity selection texture colored with unity selection color.
        /// </summary>
        public static Texture2D UnitySelectionTexture = new Texture2D(1, 1);
        #endregion
        
        /// <summary>
        /// Initializes static members of the <see cref="KYIEditorStyles" /> class.
        /// </summary>
        static KYIEditorStyles()
        {
            InitializeTextures();
            InitializeGUIStyles();
        }

        /// <summary>
        /// Initializes the textures.
        /// </summary>
        private static void InitializeTextures()
        {
            UnitySelectionTexture.SetPixel(0, 0, KYIColorPreset.UnitySelection);
            UnitySelectionTexture.Apply();
        }
        /// <summary>
        /// Initializes the GUI styles.
        /// </summary>
        private static void InitializeGUIStyles()
        {
            _preferencSelection = new GUIStyle("PreferencesSection");
            _preferencSelection.onNormal.background = UnitySelectionTexture;

            _tableHeader = new GUIStyle("RL Header");
            _tableHeader.alignment = TextAnchor.MiddleCenter;

            _midLabel = new GUIStyle("label");
            _midLabel.alignment = TextAnchor.MiddleCenter;

            _kyisaveSubMenuHeader = new GUIStyle("MeTransitionHead");

            _reorderableListBackground = new GUIStyle("RL Background");
            _reorderableListBackground.stretchHeight = false;

            _groupBox = new GUIStyle("GroupBox");

        }
    }
}