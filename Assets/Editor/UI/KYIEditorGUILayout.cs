﻿// ***********************************************************************
// Assembly         : Assembly-CSharp-Editor
// Author           : IN-YEOL, CHOI
// Created          : 10-21-2016
//
// Last Modified By : IN-YEOL, CHOI
// Last Modified On : 10-22-2016
// ***********************************************************************
// <copyright file="KYIEditorGUILayout.cs" company="KYIGames">
//     Copyright (c) KYIGames. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using UnityEngine;
using UnityEditor;
using KYIEngine;
using System;
using System.Collections.Generic;

namespace KYIEditor
{
    /// <summary>
    /// KYIEditorGUILayout is helper class containing extended version of unity's <see cref="EditorGUILayout" />.
    /// </summary>
    public static class KYIEditorGUILayout
    {
        #region ToggleSelectionButton
        /// <summary>
        /// The selection options
        /// </summary>
        private static readonly string[] _selectionOptions = { "Off", "On" };
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <param name="buttonStyle">The button style.</param>
        /// <param name="options">The options.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(bool selected, GUIContent label, GUIStyle buttonStyle, params GUILayoutOption[] options)
        {
            using (var horizontalScope = new EditorGUILayout.HorizontalScope(options))
            {
                int selectedIndex = selected ? 1 : 0;
                EditorGUILayout.PrefixLabel(label);
                selectedIndex = GUILayout.SelectionGrid(selectedIndex, _selectionOptions, 2, buttonStyle);
                return selectedIndex == 1 ? true : false;
            }
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <param name="options">The options.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(bool selected, GUIContent label, params GUILayoutOption[] options)
        {
            return ToggleSelectionButton(selected, label, EditorStyles.miniButton, options);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="options">The options.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(bool selected, params GUILayoutOption[] options)
        {
            return ToggleSelectionButton(selected, GUIContent.none, options);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <param name="buttonStyle">The button style.</param>
        /// <param name="options">The options.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(bool selected, string label, GUIStyle buttonStyle, params GUILayoutOption[] options)
        {
            return ToggleSelectionButton(selected, new GUIContent(label), buttonStyle, options);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <param name="options">The options.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(bool selected, string label, params GUILayoutOption[] options)
        {
            return ToggleSelectionButton(selected, label, EditorStyles.miniButton, options);
        }
        #endregion
        
        #region LabelFieldWithBackgroundColor
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            GUIContent label,
            GUIContent label2,
            GUIStyle style,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            KYIEditorGUI.LabelFieldWithBackgroundColor(EditorGUILayout.GetControlRect(options), label, label2, style, backgroundColor);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            GUIContent label,
            GUIStyle style,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            KYIEditorGUI.LabelFieldWithBackgroundColor(EditorGUILayout.GetControlRect(options), label, style, backgroundColor);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            GUIContent label,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            LabelFieldWithBackgroundColor(label, EditorStyles.label, backgroundColor, options);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            string label,
            string label2,
            GUIStyle style,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            LabelFieldWithBackgroundColor(new GUIContent(label), new GUIContent(label2), style, backgroundColor, options);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            string label,
            string label2,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            LabelFieldWithBackgroundColor(label, label2, EditorStyles.label, backgroundColor, options);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void LabelFieldWithBackgroundColor(
            string label,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            LabelFieldWithBackgroundColor(new GUIContent(label), backgroundColor, options);
        }
        #endregion

        #region CenterdLabelField
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            GUIContent label,
            GUIContent label2,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            KYIEditorGUI.CenteredLabelField(EditorGUILayout.GetControlRect(options), label, label2, backgroundColor);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            GUIContent label,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(label, GUIContent.none, backgroundColor, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            GUIContent label,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(label, Color.clear, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            GUIContent label,
            GUIContent label2,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(label, label2, Color.clear, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            string label,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(new GUIContent(label), GUIContent.none, Color.clear, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            string label,
            string label2,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(new GUIContent(label), GUIContent.none, Color.clear, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            string label,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(new GUIContent(label), GUIContent.none, backgroundColor, options);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        /// <param name="options">The options.</param>
        public static void CenteredLabelField(
            string label,
            string label2,
            Color backgroundColor,
            params GUILayoutOption[] options)
        {
            CenteredLabelField(new GUIContent(label), new GUIContent(label2), backgroundColor, options);
        }
        #endregion

        public static bool Foldout(bool foldout, string label, Color backgroundColor, bool toggleOnLabelClick = true, params GUILayoutOption[] options)
        {
            var controlRect = EditorGUILayout.GetControlRect(options);
            return KYIEditorGUI.Foldout(controlRect, foldout, label, backgroundColor, toggleOnLabelClick);
        }

        public static bool Foldout(bool foldout, SerializedProperty property, bool toggleOnLabelClick = true, params GUILayoutOption[] options)
        {
            return KYIEditorGUI.Foldout(EditorGUILayout.GetControlRect(options), foldout, property, toggleOnLabelClick);
        }
        public static bool Foldout(bool foldout, SerializedProperty property, Color backgroundColor, bool toggleOnLabelClick = true, params GUILayoutOption[] options)
        {
            return KYIEditorGUI.Foldout(EditorGUILayout.GetControlRect(options), foldout, property, backgroundColor, toggleOnLabelClick);
        }
        // private static Stack<LayoutScope> _layoutScope = new Stack<LayoutScope>();
        public sealed class HorizontalScope : ILayoutScope
        {
            private Rect _scope;
            private Rect _lastRect;
            private float _totalWidth;
            private float _remainingWidth;

            public HorizontalScope(Rect scope)
            {
                _scope = scope;
                _lastRect = _scope;
                _lastRect.width = 0.0f;
                _totalWidth = _scope.width;
                _remainingWidth = _totalWidth;
            }
            public Rect GetScope()
            {
                return _scope;
            }
            public Rect GetRemainingRect()
            {
                return new Rect(_lastRect.x + _lastRect.width, _scope.y, _remainingWidth, _scope.height);
            }
            public Rect GetRect(float width)
            {
                var current = new Rect(_lastRect.x + _lastRect.width, _scope.y, width, _scope.height);
                _lastRect = current;
                _remainingWidth -= width;
                return current;
            }
            public Rect GetRectRatio(float ratio)
            {
                return GetRect(_scope.width * ratio);
            }
            public Rect GetLastRect()
            {
                return _lastRect;
            }
            public Rect GetLabelRect()
            {
                return GetRect(EditorGUIUtility.labelWidth - (EditorGUI.indentLevel * KYIEditorGUIUtility.WidthPerIndent));
            }
            public Rect GetLabelRect(int indentLevel)
            {
                return GetRect(EditorGUIUtility.labelWidth - (indentLevel * KYIEditorGUIUtility.WidthPerIndent));
            }
            public Rect GetRectEven(int count)
            {
                return GetRect(_remainingWidth / count);
            }
            public Rect GetRectFromEnd(float width)
            {
                GetRect(_remainingWidth - width);
                return GetRect(width);
            }
            /// <summary>
            /// Not so meaningful to do this. Just get better the readability when writing GUI layout code.
            /// </summary>
            public void Dispose()
            {
                GC.SuppressFinalize(this);
            }
        }

    }
}