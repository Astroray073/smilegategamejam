﻿using UnityEditor;
using UnityEngine;

namespace KYIEditor
{
    public static class KYIEditorGUIUtility
    {
        public static float WidthPerIndent { get { return 15.0f; } }

        public static Rect GetLabelRect(Rect controlRect)
        {
            var indentCorrection = EditorGUI.indentLevel * WidthPerIndent;
            return new Rect(controlRect.x, controlRect.y, EditorGUIUtility.labelWidth - indentCorrection, controlRect.height);
        }
    }
}