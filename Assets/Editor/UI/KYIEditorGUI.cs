﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using KYIEngine;

namespace KYIEditor
{
    /// <summary>
    /// KYIEditorGUI is helper class containing extended version of unity's <see cref="EditorGUI" />.
    /// </summary>
    public static class KYIEditorGUI
    {
        #region ToggleSelectionButton
        /// <summary>
        /// The selection options
        /// </summary>
        private static readonly string[] _selectionOptions = { "Off", "On" };
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> when button is on.</param>
        /// <param name="label">The label.</param>
        /// <param name="buttonStyle">The button style.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(
            Rect position,
            bool selected,
            GUIContent label,
            GUIStyle buttonStyle)
        {
            int selectedIndex = selected ? 1 : 0;
            EditorGUI.PrefixLabel(position, label);
            position.width -= EditorGUIUtility.labelWidth;
            position.x += EditorGUIUtility.labelWidth;
            selectedIndex = GUI.SelectionGrid(position, selectedIndex, _selectionOptions, 2, buttonStyle);
            return selectedIndex == 1 ? true : false;
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(
            Rect position,
            bool selected,
            GUIContent label)
        {
            return ToggleSelectionButton(position, selected, label, EditorStyles.miniButton);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="buttonStyle">The button style.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>

        public static bool ToggleSelectionButton(
            Rect position,
            bool selected,
            GUIStyle buttonStyle)
        {
            return ToggleSelectionButton(position, selected, GUIContent.none, buttonStyle);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <param name="buttonStyle">The button style.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(
            Rect position,
            bool selected,
            string label,
            GUIStyle buttonStyle)
        {
            return ToggleSelectionButton(position, selected, new GUIContent(label), buttonStyle);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <param name="label">The label.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(
            Rect position,
            bool selected,
            string label)
        {
            return ToggleSelectionButton(position, selected, new GUIContent(label), EditorStyles.miniButton);
        }
        /// <summary>
        /// Toggles the selection button.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="selected">if set to <c>true</c> [selected].</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool ToggleSelectionButton(
            Rect position,
            bool selected)
        {
            return ToggleSelectionButton(position, selected, GUIContent.none, EditorStyles.miniButton);
        }
        #endregion
        
        #region LabelFieldWithBackgroundColor
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            GUIContent label,
            GUIContent label2,
            GUIStyle style,
            Color backgroundColor)
        {
            DrawBoxWithColor(position, backgroundColor);
            EditorGUI.LabelField(position, label, label2, style);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            GUIContent label,
            GUIStyle style,
            Color backgroundColor)
        {
            DrawBoxWithColor(position, backgroundColor);
            EditorGUI.LabelField(position, label, style);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            GUIContent label,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, label, EditorStyles.label, backgroundColor);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="style">The style.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            string label,
            string label2,
            GUIStyle style,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, new GUIContent(label), new GUIContent(label2), style, backgroundColor);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            string label,
            string label2,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, label, label2, EditorStyles.label, backgroundColor);
        }
        /// <summary>
        /// Labels the color of the field with background.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void LabelFieldWithBackgroundColor(
            Rect position,
            string label,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, new GUIContent(label), backgroundColor);
        }
        #endregion

        #region CenterdLabelField
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void CenteredLabelField(
            Rect position,
            GUIContent label,
            GUIContent label2,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, label, label2, KYIEditorStyles.MidLabel, backgroundColor);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void CenteredLabelField(
            Rect position,
            GUIContent label,
            Color backgroundColor)
        {
            LabelFieldWithBackgroundColor(position, label, KYIEditorStyles.MidLabel, backgroundColor);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        public static void CenteredLabelField(
            Rect position,
            GUIContent label)
        {
            CenteredLabelField(position, label, Color.clear);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        public static void CenteredLabelField(
            Rect position,
            GUIContent label,
            GUIContent label2)
        {
            CenteredLabelField(position, label, label2, Color.clear);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        public static void CenteredLabelField(
            Rect position,
            string label)
        {
            CenteredLabelField(position, new GUIContent(label), Color.clear);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        public static void CenteredLabelField(
            Rect position,
            string label,
            string label2)
        {
            CenteredLabelField(position, new GUIContent(label), new GUIContent(label2), Color.clear);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void CenteredLabelField(
            Rect position,
            string label,
            Color backgroundColor)
        {
            CenteredLabelField(position, new GUIContent(label), backgroundColor);
        }
        /// <summary>
        /// Centereds the label field.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="label">The label.</param>
        /// <param name="label2">The label2.</param>
        /// <param name="backgroundColor">Color of the background.</param>
        public static void CenteredLabelField(
            Rect position,
            string label,
            string label2,
            Color backgroundColor)
        {
            CenteredLabelField(position, new GUIContent(label), new GUIContent(label2), backgroundColor);
        }
        #endregion

        public static bool Foldout(Rect rect, bool foldout, string label, Color backgroundColor, bool toggleOnLabelClick = true)
        {
            DrawBoxWithColor(rect, backgroundColor);
            return EditorGUI.Foldout(rect, foldout, label, toggleOnLabelClick);
        }

        public static bool Foldout(Rect rect, bool foldout, SerializedProperty property, bool toggleOnLabelClick = true)
        {
            var result = EditorGUI.Foldout(KYIEditorGUIUtility.GetLabelRect(rect), foldout, GUIContent.none, toggleOnLabelClick);
            EditorGUI.PropertyField(rect, property);
            return result;
        }
        public static bool Foldout(Rect rect, bool foldout, SerializedProperty property, Color backgroundColor, bool toggleOnLabelClick = true)
        {
            DrawBoxWithColor(rect, backgroundColor);
            var result = EditorGUI.Foldout(KYIEditorGUIUtility.GetLabelRect(rect), foldout, GUIContent.none);
            EditorGUI.PropertyField(rect, property);
            return result;
        }

        public static void DrawBoxWithColor(Rect rect, Color color)
        {
            rect = EditorGUI.IndentedRect(rect);
            GUI.backgroundColor = color;
            GUI.Box(rect, GUIContent.none);
            GUI.backgroundColor = Color.white;
        }
    }
}