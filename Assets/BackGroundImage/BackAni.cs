﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BackAni : MonoBehaviour {


    public float BackMoveTime;
    public float GameTime;
    public Vector3 Move;
    public int BackLevel;
    public bool BackMove;

    // Use this for initialization
    void Start()
    {
        Move = Vector3.zero;
        BackLevel = 0;
        BackMoveTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (BackMoveTime > GameTime / 5 && !BackMove && BackLevel < 4)
        {
            Move.y = 6.5f * Time.deltaTime;
            BackMoveTime = 0;
            BackMove = true;
            BackLevel++;
        }

        if(BackMove)
        {
            transform.position -= Move;
        }

        if (BackMoveTime > 1f)
        {
            BackMove = false;
        }

        BackMoveTime += Time.fixedDeltaTime;


        // UI 업데이트
        int time = (int)(GameTime / 5) - (int)BackMoveTime;
        if (time <= 0) time = 0;
        GameManager.Gamemanager.UI.transform.FindChild("Time_min").GetComponent<Text>().text = (time / 60).ToString();
        GameManager.Gamemanager.UI.transform.FindChild("Time_sec").GetComponent<Text>().text = time.ToString();

    }
}
