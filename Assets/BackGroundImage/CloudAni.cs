﻿using UnityEngine;
using System.Collections;

public class CloudAni : MonoBehaviour {

    public float RowMoveTime;
    public Vector3 Move;
    public Vector3 ResetPosition;
    public float RowMove = 1;

    // Use this for initialization
    void Start()
    {
        ResetPosition = transform.position;
        Move.z = 0;
        Move.x = -Time.deltaTime / 2;
        RowMoveTime = 0;
        transform.position = transform.position + new Vector3(0, 0, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if(RowMoveTime <= 0)
        {
            RowMoveTime = Random.RandomRange(0.5f, 1f);
            RowMove *= -1;
        }
        else
        {
            RowMoveTime -= Time.fixedDeltaTime;
            Move.y = RowMove * 0.2f * Time.fixedDeltaTime;
        }
        transform.position += Move;
        if (transform.position.x > 40f)
            transform.position = ResetPosition;
    }
}
